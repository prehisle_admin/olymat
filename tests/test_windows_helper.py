import pytest

from olymat.utils.windows_helper import WindowsCommandManager, TimeoutException


@pytest.fixture(params=[
    "cmd",
    "powershell"
])
def cmd_manager(request):
    # 根据参数启动不同的终端
    manager = WindowsCommandManager(terminal=request.param)
    yield manager
    manager.close()


def test_run_simple_command(cmd_manager):
    output = cmd_manager.run_command("echo Hello")
    assert "Hello" in output, "Failed to find expected output 'Hello'"


def test_wait_for_specific_character(cmd_manager):
    output = cmd_manager.run_command("ping -n 1 127.0.0.1", wait_for="TTL=")
    assert "TTL=" in output, "Failed to find 'TTL=' in the output"
    output = cmd_manager.run_command("ping -n 1 127.0.0.1", wait_for=b"TTL=")
    assert "TTL=" in output, "Failed to find 'TTL=' in the output"


def test_run_command_with_timeout(cmd_manager):
    with pytest.raises(TimeoutException, match="Command timed out after 2 seconds"):
        cmd_manager.run_command("ping -n 10 127.0.0.1", timeout=2)



