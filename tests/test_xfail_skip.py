import pytest


# 需在pytest.ini中去除-x, val为5时为跳过
@pytest.mark.parametrize("val", [1, 2, 3, 4, 5])
def test_xfail_skip_demo(val):
    if val > 3:
        assert False
