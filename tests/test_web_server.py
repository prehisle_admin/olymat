import pytest
import io

from olymat.webserver import olymat_webserver


@pytest.fixture
def client():
    olymat_webserver.config['TESTING'] = True
    with olymat_webserver.test_client() as client:
        yield client


def test_upload_file_success(client):
    data = {
        'file': (io.BytesIO(b"test file content"), 'test_file.txt'),
        'target_path': 'test_dir/test_file.txt'
    }
    response = client.post('/api/upload', data=data, content_type='multipart/form-data')
    json_data = response.get_json()

    assert response.status_code == 200
    assert json_data['code'] == 0
    assert json_data['msg'] == "File uploaded successfully"
    assert 'file_path' in json_data['data']


def test_upload_file_no_file(client):
    data = {
        'target_path': 'test_dir/test_file.txt'
    }
    response = client.post('/api/upload', data=data)
    json_data = response.get_json()

    assert response.status_code == 200
    assert json_data['code'] == 1
    assert json_data['msg'] == "No file part in the request"


def test_upload_file_no_target_path(client):
    data = {
        'file': (io.BytesIO(b"test file content"), 'test_file.txt')
    }
    response = client.post('/api/upload', data=data, content_type='multipart/form-data')
    json_data = response.get_json()

    assert response.status_code == 200
    assert json_data['code'] == 1
    assert json_data['msg'] == "Target path not specified"
