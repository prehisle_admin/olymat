import os.path
import re
import shutil

import pytest

from olymat.utils.file_helper import replace_data, replace_data_bin
from olymat.utils.utils import unzip_file
from olymat.utils.windows_helper import WindowCmdWrap


# 要在olymat项目中运行


@pytest.fixture()
def cmd():
    cmd = WindowCmdWrap(["cmd", "/k"], shell=False)
    cmd.exec("chcp 65001")
    cmd.exec("deactivate", wait_str=">")
    cmd.exec("set prompt=$lolym$g")
    cmd.exec("set LIBRARY_ROOTS=")
    cmd.exec("set PYTHONPATH=")
    # cmd.exec("set")
    return cmd


@pytest.fixture(scope="module")
def VERSION():
    return open("version").readline().strip()


def test_unzip(cmd):
    cmd.exec("cd embed_python_build")
    cmd.exec("cd")
    cmd.exec(rf"rmdir /s /q olymat_cli")
    assert not os.path.isdir("embed_python_build/olymat_cli")
    cmd.exec("7z x python-3.11.2-embed-amd64.zip -oolymat_cli/python")
    # cmd.exec("7z x python-3.11.2-embed-win32.zip -oolymat_cli/python")


def test_fix(cmd):
    cmd.exec(r"cd embed_python_build\olymat_cli\python")
    cmd.exec("mkdir Scripts")
    cmd.exec("mkdir Lib")


def test_fix2(cmd):
    cmd.exec("cd embed_python_build")
    cmd.exec("copy pip.pyz olymat_cli\python /Y")
    cmd.exec("copy virtualenv.pyz olymat_cli\python /Y")
    cmd.exec("copy python311._pth olymat_cli\python /Y")


def test_(cmd):
    cmd.exec(r"cd embed_python_build\olymat_cli\python")
    cmd.exec("set path=%cd%;%cd%\Scripts")
    cmd.exec("python pip.pyz install pip==23.0.1 -i https://pypi.tuna.tsinghua.edu.cn/simple")
    cmd.exec("python pip.pyz install virtualenv==20.21.0 -i https://pypi.tuna.tsinghua.edu.cn/simple")


def test_rm_pyz(cmd):
    cmd.exec("cd embed_python_build")
    cmd.exec("del olymat_cli\python\pip.pyz /Q")
    cmd.exec("del olymat_cli\python\pvirtualenv.pyz /Q")


def test_install_olymat(cmd):
    olymat_dir = os.getcwd()
    cmd.exec(r"cd embed_python_build\olymat_cli\python")
    cmd.exec(r"set path=%cd%;%cd%\Scripts;C:\Program Files\Git\cmd")
    cmd.exec(
        r"pip install setuptools==67.6.1 -i https://pypi.tuna.tsinghua.edu.cn/simple")
    # cmd.exec(
    #     r"pip install wheel -i https://pypi.tuna.tsinghua.edu.cn/simple")
    cmd.exec(
        rf"pip install {olymat_dir} -i https://pypi.tuna.tsinghua.edu.cn/simple",
    )


def test_olymat_cli_bat(cmd):
    cmd.exec("cd embed_python_build")
    cmd.exec("copy olymat_cli.bat olymat_cli")
    cmd.exec("xcopy min-git olymat_cli\min-git /s /e /i /h /f /Y")


def test_clear_pycache(cmd):
    def clear_pycache(dir):
        for root, dirs, files in os.walk(dir):
            for dir in dirs:
                if dir == "__pycache__":
                    full_dir = os.path.join(root, dir)
                    print(full_dir)
                    shutil.rmtree(full_dir)
            for file in files:
                if file == "installed-files.txt":
                    full_file = os.path.join(root, file)
                    print(full_file)
                    os.remove(full_file)

    clear_pycache(r"embed_python_build/olymat_cli")


def test_file_script_path():
    olymat_dir = os.getcwd()
    replace_data(rf"{olymat_dir}\embed_python_build\olymat_cli\python\Scripts\olymat-script.py",
                 "^.*?embed_python_build.*?$", "", flags=re.MULTILINE)
    replace_data(rf"{olymat_dir}\embed_python_build\olymat_cli\python\Scripts\olymat_cli-script.py",
                 "^.*?embed_python_build.*?$", "", flags=re.MULTILINE)
    file_all_exe()


def file_all_exe():
    olymat_dir = os.getcwd()
    root_dir = rf"{olymat_dir}\embed_python_build\olymat_cli\python\Scripts"
    fl = os.listdir(root_dir)
    for filename in fl:
        if filename.endswith(".exe"):
            full_path = os.path.join(root_dir, filename)
            print(full_path)
            find_b = rf"#!{olymat_dir}\embed_python_build\olymat_cli\python\python.exe".encode()
            r_b = rf"#!python.exe".ljust(len(find_b)).encode()
            replace_data_bin(full_path, find_b, r_b)


def copy_files(source_dir, dest_dir):
    for root, _, files in os.walk(source_dir):
        relative_path = os.path.relpath(root, source_dir)
        target_dir = os.path.join(dest_dir, relative_path)
        os.makedirs(target_dir, exist_ok=True)

        for file in files:
            source_file = os.path.join(root, file)
            target_file = os.path.join(target_dir, file)
            shutil.copy2(source_file, target_file)  # Use shutil.copy if you don't need to preserve file metadata


def test_fix_tk():
    copy_files("embed_python_build/fix_tk_3.11.2", "embed_python_build/olymat_cli/python")


def test_place_olymat_cli(cmd, VERSION):
    cmd.exec(rf"rmdir /s /q olymat_cli\olymat_cli_{VERSION}")
    assert not os.path.isdir(rf"olymat_cli\olymat_cli_{VERSION}")
    cmd.exec("cd embed_python_build")
    cmd.exec(rf"xcopy olymat_cli ..\olymat_cli\olymat_cli_{VERSION} /e /i /h /f /Y")


def test_unzip_python_zip(cmd, VERSION):
    unzip_file(f"olymat_cli\olymat_cli_{VERSION}\python\python311.zip", f"olymat_cli\olymat_cli_{VERSION}\python")
# def test_zip_compress(cmd, VERSION):
#     cmd.exec(rf"cd olymat_cli")
#     cmd.exec(rf"del olymat_cli_{VERSION}.zip")
#     cmd.exec(rf"7z a olymat_cli_{VERSION}.zip .\olymat_cli_{VERSION}")
