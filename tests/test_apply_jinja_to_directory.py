import os
import shutil
import unittest

from olymat.utils.jinja2_helper import apply_jinja_to_directory


class TestJinja2Helper(unittest.TestCase):
    def setUp(self):
        self.src_dir = 'test_src'
        self.dst_dir = 'test_dst'
        self.conf = {'name': 'TestProject', 'version': '1.0'}

        # Create source directory and files
        os.makedirs(self.src_dir)
        with open(os.path.join(self.src_dir, 'file1.txt'), 'w', encoding='utf-8') as f:
            f.write('Project Name: {{ name }}\nVersion: {{ version }}')  # 修改内容以匹配预期

        # Ensure the subdir exists before creating the file
        os.makedirs(os.path.join(self.src_dir, 'subdir'), exist_ok=True)
        with open(os.path.join(self.src_dir, 'subdir', 'file2.txt'), 'w', encoding='utf-8') as f:
            f.write('This is a subdirectory file in {{ name }}.')

    def tearDown(self):
        # Clean up test directories
        shutil.rmtree(self.src_dir)
        if os.path.exists(self.dst_dir):
            shutil.rmtree(self.dst_dir)

    def test_apply_jinja_to_directory(self):
        apply_jinja_to_directory(self.src_dir, self.dst_dir, self.conf)

        # Check if the destination directory and files exist
        self.assertTrue(os.path.exists(os.path.join(self.dst_dir, 'file1.txt')))
        self.assertTrue(os.path.exists(os.path.join(self.dst_dir, 'subdir', 'file2.txt')))

        # Check the content of the files
        with open(os.path.join(self.dst_dir, 'file1.txt'), 'r', encoding='utf-8') as f:
            content1 = f.read()
            self.assertEqual(content1, 'Project Name: TestProject\nVersion: 1.0')

        with open(os.path.join(self.dst_dir, 'subdir', 'file2.txt'), 'r', encoding='utf-8') as f:
            content2 = f.read()
            self.assertEqual(content2, 'This is a subdirectory file in TestProject.')


if __name__ == '__main__':
    unittest.main()
