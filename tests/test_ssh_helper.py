import os
import textwrap

import pytest
from dotenv import load_dotenv

import olymat
from olymat.utils.ssh_helper import ssh_chan

@pytest.fixture(autouse=True, scope="session")
def env_init():
    load_dotenv()

@pytest.fixture
def ssh():

    return ssh_chan({
        "ip": os.getenv('HOST'),
        "username": os.getenv('USER'),
        "password": os.getenv('PASS'),
    }, timeout=3)


def test_send(ssh):
    ssh.raw_send(b"ls\n")
    ssh.raw_except(b"# ")
    ssh.raw_send("ls\n")
    ssh.raw_except("# ")
    ssh.raw_send(b"ls\n")
    with pytest.raises(olymat.utils.control_helper.TimeoutException):
        ssh.raw_except(b"xxx", timeout=1)


def test_raw_except_list(ssh):
    ssh.raw_send(b"ls /\n")
    ssh.raw_except(b"boot", is_in=True)
    ssh.raw_send(b"ls /\n")
    ssh.raw_except([b"boot"], is_in=True)
    ssh.raw_send(b"ls /\n")
    ssh.raw_except([b"boot", b"lib64", b"~# "], is_in=True, is_check_end=True)
    ssh.raw_send(b"ls /\n")
    ssh.raw_except(["boot", "lib64", "~# "], is_in=True, is_check_end=True)


def test_wait_for_single_byte(ssh):
    ssh.raw_send(b"ls /\n")
    ret, match_s = ssh.wait_for_single_byte(b"etc")
    print("\n", ret, match_s)


def test_wait_for_multiple_bytes(ssh):
    ssh.raw_send(b"ls /\n")
    ret, match_s = ssh.wait_for_multiple_bytes([b"aaa", b"mnt", b"xxxxx"])
    assert b"mnt" == match_s

    ssh.raw_send(b"ls /\n")
    ret, match_s = ssh.wait_for_multiple_str(["etc"])
    print(match_s)

def test_wait_for_regex(ssh):
    ssh.raw_send(b"ls /\n")
    ret, match_s = ssh.wait_for_regex(["aaa", "mnt", "xxxxx"])
    print("\n", ret)
    print("\n", match_s)

def test_timeout(ssh):
    ssh.raw_send(b"ls\n")
    with pytest.raises(olymat.utils.control_helper.TimeoutException) as e:
        ssh.raw_except(b"xxxx", timeout=2)
    assert e.value.args[0] == "waiting timeout 2"

    ssh.raw_send('while true; do echo -n "*"; sleep 1; done')
    with pytest.raises(olymat.utils.control_helper.TimeoutException) as e:
        ssh.raw_except(b"xxxx", timeout=2)
    assert e.value.args[0] == "running timeout 2"


def test_send_wait(ssh):
    ssh.raw_send_wait(b"ls\n", "# ")
    ssh.raw_send_wait(b"ls\n", "# ", is_check_end=True)
    with pytest.raises(olymat.utils.control_helper.TimeoutException) as e:
        ssh.raw_send_wait("ls /", [b"aaa", b"mnt", b"xxxxx"], is_check_end=True, timeout=2)


def test_multi_exec(ssh):
    ssh.raw_send_wait_multi(textwrap.dedent("""
    ls
    ls
    """))

def test_raw_seq_list(ssh):
    ssh.raw_send(b"ls /\n")
    ssh.raw_except(b"boot", is_in=False)
    ssh.raw_except(b"dev", is_in=False)
    ssh.raw_except(b"root@OpenWrt:~# ", is_in=False)

    ssh.raw_send(b"ls /\n")
    ssh.raw_except(b"dev", is_in=False)
    with pytest.raises(olymat.utils.control_helper.TimeoutException):
        ssh.raw_except(b"boot", is_in=False, timeout=2)
    # ssh.raw_except(b"root@OpenWrt:~# ", is_in=False)

def test_exec_chan8(ssh):
    ssh.exec_chan8("ls /mnt")
    ssh.exec_chan8("ls /mnt", "sd.128", re_match=True)
    ssh.exec_chan8("mkdir /tmp/好1的2**长")

def test_exec_chan8_2(ssh):
    ssh.exec_chan8("ls /tmp", "好1.*?长", re_match=True)

def test_raw_send_wait2(ssh):
    r, m = ssh.raw_send_wait2("ls /tmp", ["1", "2"])
    print("*"*80)
    print(r, m)
    assert m == "1"
    r, m = ssh.raw_send_wait2("ls /tmp", ["11", "好1的2**长"])
    print("*"*80)
    print(r, m)
    assert m == "好1的2**长"
    r, m = ssh.raw_send_wait2("ls /tmp", [])
    print("*"*80)
    print(r, m)
    assert m == ssh.get_until_str()

    r, m = ssh.raw_send_wait2("ls /tmp", "好1的2**长")
    print("*"*80)
    print(r, m)
    assert m == "好1的2**长"


def test_sshtunnel():
    from sshtunnel import SSHTunnelForwarder

    server = SSHTunnelForwarder(
        os.getenv('HOST'),
        ssh_username=os.getenv('USER'),
        ssh_password=os.getenv('PASS'),
        remote_bind_address=('192.168.23.5', 22)
    )

    server.start()

    print(server.local_bind_port)  # show assigned local port
    # work with `SECRET SERVICE` through `server.local_bind_port`.

    server.stop()


def test_raw_send_wait3(ssh):
    r, m = ssh.raw_send_wait3("ls /", ["xx1", "lib64"])
    assert m == "lib64"

    ssh.recv_buffer_fix() # 上条命令有未接收完的数据，需要继续接收一次，避免和后续的命令混淆

    r, m = ssh.raw_send_wait3("ls /")
    assert m == "# "

    m = ssh.raw_send_wait_simple("ls /", ["xx1", "lib64"])
    assert m == "lib64"

    ssh.recv_buffer_fix() # 上条命令有未接收完的数据，需要继续接收一次，避免和后续的命令混淆

    m = ssh.raw_send_wait_simple("ls /")
    assert m == "# "


    r, m = ssh.raw_send_wait3("ls /")
    assert m == "# "

    r, m = ssh.raw_send_wait3("ls /", ["xx1", "lib64"], append_default_until_str=False)
    assert m == "lib64"

    ssh.recv_buffer_fix() # 上条命令有未接收完的数据，需要继续接收一次，避免和后续的命令混淆

    r, m = ssh.raw_send_wait3("ls /")
    assert m == "# "

    r, m = ssh.raw_send_wait3("ls /", append_default_until_str=False)
    assert m == "# "

    ssh.recv_buffer_fix() # 上条命令有未接收完的数据，需要继续接收一次，避免和后续的命令混淆

    r, m = ssh.raw_send_wait3("ls /")
    assert m == "# "