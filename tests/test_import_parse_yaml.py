import os

import pytest

from olymat.utils.dict_helper import DotDict
from olymat.utils.dir_helper import make_rel_path
from olymat.utils.yaml_helper import save_yaml, load_yaml, import_parse_yaml


@pytest.fixture(scope="session")
def data():
    data = import_parse_yaml(make_rel_path(__file__, "datas/root.yml"))
    root_new = make_rel_path(__file__, "datas/root_new.yml")
    save_yaml(root_new, data)
    return DotDict(load_yaml(root_new))




def test_1(data):
    assert data.root == 0
    assert data.sub == 1
    assert data.sub2 == 2
    assert data["sub2.1"] == 2.1

def test_over(data):
    assert data.rootA == 0
    assert data.rootB == "0"
    assert data.rootC == {'a': 1}
    assert data.rootD == {}
    assert data.rootDD == 123
    assert data.rootE.a == 1

@pytest.fixture(scope="session")
def data2():
    data = import_parse_yaml(make_rel_path(__file__, "datas/root2.yml"))
    root_new = make_rel_path(__file__, "datas/root_new2.yml")
    save_yaml(root_new, data)
    return DotDict(load_yaml(root_new))

def test_over2(data2):
    assert data2.rootF.a == 1
    assert data2.rootF.b == 1
    assert data2.rootG.a.b.e == 2
    assert data2.rootG.a.c == 2
    assert data2.rootG.b == "xxx"
    assert data2.env_info.name == "赛哥的 204.49 虚机环境"
    assert data2.env_info.servers.cp1.ssh.ip == "192.168.204.xx"
    assert data2.env_info.b == "xxx"



@pytest.fixture(scope="session")
def data3():
    data = import_parse_yaml(make_rel_path(__file__, "datas/root3.yml"))
    root_new = make_rel_path(__file__, "datas/root_new3.yml")
    save_yaml(root_new, data)
    return DotDict(load_yaml(root_new))

def test_over3(data3):
    assert data3.env_info.name == "赛哥的 204.49 虚机环境"
    assert data3.env_info.servers.cp1.ssh.ip == "192.168.204.xx"
    assert data3.env_info.servers.cp2.ssh.password == "xxxxxx"