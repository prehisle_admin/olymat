import pytest


@pytest.mark.case_infos(creator="小明",
                        create_time="2021-08-18",
                        update_time="2021-08-18",
                        name="升级",
                        step="上传包",
                        func_ids="",
                        severity="blocker",
                        interface_ids="API_0202",
                        tags="功能测试,接口测试,异常测试",
                        issue="#45",
                        desc="签名验签"
                        )
def test_passed():
    print('passed')


@pytest.mark.case_infos('''
creator: "小明"
create_time: "2021-08-18"
update_time: "2021-08-18"
name: "升级"
step: "上传包"
func_ids: ""
severity: "blocker"
interface_ids: "API_0202"
tags: "功能测试,接口测试,异常测试"
issue: "#45"
desc: "签名验签"
''')
def test_passed_yaml():
    print('passed')
