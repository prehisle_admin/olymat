import time

from olymat.utils.control_helper import RetryException, call_until_success_timeout


@call_until_success_timeout()
def f():
    time.sleep(1)
    raise RetryException()


def test_f():
    f()