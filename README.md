# olymat是什么

olymat是一个python通用自动化框架，继承自pytest，适用于测试、运维、工具开发。olymat旨在降低门槛，增加效率，探索python自动化最佳实践。
olymat是奥联内部的主力自动化工具。

# olymat特性

- 简洁的示例代码，快速上手
- 内置大量助手库和示例，开箱即用
- 有一个外挂UI(olymat-ui), 可以快速分享自动化脚本
- 友好的社区，即时交流


# 交流

0. 有任何问题，欢迎提issue和pr
1. 微信群,加我进群  
   ![微信群](imgs/weixinqun.png)

# 常用链接

* [olymat-ui](https://gitee.com/prehisle_admin/olymat-ui)
* [olymat官方工具、示例库](https://gitee.com/prehisle_admin/olymat-tools)
* [olymat项目快速上手初始项目模板](https://gitee.com/prehisle_admin/olymat-project-tpl)


