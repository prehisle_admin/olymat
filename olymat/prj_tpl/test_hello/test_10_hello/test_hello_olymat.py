def test_hello_cfg(cfg):
    print(f"hello {cfg.get_ping()}!")
    print(f"hello {cfg.ping}!")
    assert cfg.ping == "pong"


def test_hello_ctx(ctx):
    ctx.a = 1


def test_hello_ctx_2(ctx):
    assert ctx.a == 1


def test_main_ssh(main_ssh):
    main_ssh.exec_chan8("ls")


def test_dev_push(pkgs_ssh):
    pkgs_ssh.exec_chan8("ls")


def test_main_scp_to_dev_push(main_ssh, pkgs_ssh):
    main_ssh.exec_chan8("ls; pwd")
    pkgs_ssh.exec_chan8("ls; pwd")


def test_all(all_ssh):
    all_ssh.exec_chan8("ls")


def test_main_ip(main_conf):
    print(main_conf.ip)
