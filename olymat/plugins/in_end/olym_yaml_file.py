import yaml

from olymat.bases.in_end import InEndBase


class Plugin(InEndBase):
    def __init__(self, conf, mgr):
        super().__init__(conf, mgr)
        self.update_inend_data()

    def update_inend_data(self):
        self.data = {}
        for k, v in self.conf.items():
            with open(v, "r", encoding='utf-8') as f:
                self.data[k] = yaml.load(f, Loader=yaml.SafeLoader)

    def get_inend_data(self):
        return self.data
