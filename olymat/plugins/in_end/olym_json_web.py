from olymat.bases.in_end import InEndBase
from olymat.bases.mgr_base import MgrBase
from olymat.utils.olym_requests import new_session


class Plugin(InEndBase):
    def __init__(self, conf, mgr: MgrBase):
        super().__init__(conf, mgr)
        self.update_inend_data()

    def update_inend_data(self):
        s = new_session(self.mgr.get_http_proxy(), self.mgr.get_user_agent())
        self.data = {}
        for k, v in self.conf.items():
            self.data[k] = s.get(v).json()

    def get_inend_data(self):
        return self.data
