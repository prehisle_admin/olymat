import json
import os
import time

from olymat.bases.id_mgr import IdMgrBase


class DefaultIdMgr(IdMgrBase):
    def __init__(self, conf, mgr):
        super().__init__(conf, mgr)
        self.prefix = ""
        self.test_id_info = None
        if self.mgr.get_work_data_dir():
            os.makedirs(self.mgr.get_work_data_dir(), exist_ok=True)
            self.test_id_file_path = os.path.join(self.mgr.get_work_data_dir(), 'testid.json')
            today_s = time.strftime("%y%m%d", time.localtime())
            if not os.path.exists(self.test_id_file_path):
                self.write_test_id_info_to_file(today_s, 1)
            with open(self.test_id_file_path, "r") as f:
                self.test_id_info = json.load(f)

    def write_test_id_info_to_file(self, date, count):
        with open(self.test_id_file_path, "w") as f:
            json.dump({
                "date": date,
                "count": count,
            }, f)

    def get_testid(self):
        fix_prefix = self.prefix + "." if self.prefix else ""
        return f'{fix_prefix}{self.test_id_info["date"]}.{self.test_id_info["count"]:03d}'

    def inc_testid(self):
        today_s = time.strftime("%y%m%d", time.localtime())
        if today_s != self.test_id_info["date"]:
            self.test_id_info["date"] = today_s
            self.test_id_info["count"] = 1
        else:
            self.test_id_info["count"] += 1
        self.write_test_id_info_to_file(self.test_id_info["date"], self.test_id_info["count"])
        return self.get_testid()

    def update_testid(self, date, count):
        self.test_id_info["date"] = date
        self.test_id_info["count"] = int(count)
        self.write_test_id_info_to_file(self.test_id_info["date"], self.test_id_info["count"])


Plugin = DefaultIdMgr
