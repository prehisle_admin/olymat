import logging
import os

from olymat.bases.out_end import OutEndBase
from olymat.utils.csv_helper import append_dict_as_row


class Plugin(OutEndBase):
    ALLOW_EMPTY_CONF = True

    def report_test_result(self, data):
        csv_path = os.path.join(self.mgr.get_work_data_dir(), data["testid"] + ".csv")
        append_dict_as_row(csv_path, data, data.keys())

    def report_other_info(self, data):
        pass
