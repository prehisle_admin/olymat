import datetime
import logging
import time
from collections import OrderedDict

from _pytest.config import ExitCode

from olymat.bases.out_end import OutEndBase
from olymat.utils.csv_helper import read_dict_as_row
from olymat.utils.olym_requests import new_session

yj_key_map = {
    'creator': 'caseAuthor',
    'create_time': 'caseCreateTime',
    'update_time': 'caseLastUpdateTime',
    'name': 'caseName',
    'func_ids': 'functionIds',
    'interface_ids': 'interfaceIds',
    'severity': 'severity',
    'tags': 'tags',
    'issue': 'issueId',
    'desc': 'describe',
    'testid': 'testId',
    'nodeid': 'testCode',
    'test_result': 'testResult',
    'start_time': 'executeDate',
    'duration': 'duration',
    'test_type': 'testType',
}


class OlymYjOutEnd(OutEndBase):
    def __init__(self, conf, mgr):
        super().__init__(conf, mgr)
        http_proxy = self.mgr.get_http_proxy() if conf.get("enable_http_proxy", False) else None
        self.s = new_session(http_proxy)
        self.base_url = self.conf["base_url"]
        self.productId = self.mgr.get_case_extra()['productId']

    def report_test_result(self, data):
        fd = OrderedDict({})
        fd.update(self.mgr.get_case_extra())

        for k, v in yj_key_map.items():
            fd[v] = data[k]

        fd['duration'] = int(data['duration'] * 1000)
        fd['interfaceIds'] = fd['interfaceIds'].split(",")
        fd['functionIds'] = fd['functionIds'].split(",")
        fd['tags'] = fd['tags'].split(",")
        fd['caseCreateTime'] = int(datetime.datetime.strptime(fd['caseCreateTime'], "%Y-%m-%d").timestamp())
        fd['caseLastUpdateTime'] = int(datetime.datetime.strptime(fd['caseLastUpdateTime'], "%Y-%m-%d").timestamp())
        fd['executeDate'] = int(datetime.datetime.strptime(fd['executeDate'], "%Y-%m-%d %H:%M:%S").timestamp())
        logging.info("[%s]" % str(fd))

        r = self.s.post(self.base_url + "/api/data_collction/testing_result", json=fd)
        assert r.status_code == 200

    def report_other_info(self, data):
        pass

    def update_local_env_test_status(self, status):
        key = f"{self.mgr.get_case_extra()['productId']}/{self.mgr.get_testid()}/{self.mgr.get_case_extra()['envId']}"
        self.yj_data_add(key, status)

    def sessionstart(self):
        desable_test_session_status_update = self.conf.get("desable_test_session_status_update", False)
        if desable_test_session_status_update:
            return
        self.update_local_env_test_status(False)

    def sessionfinish(self, exitstatus):
        desable_test_session_status_update = self.conf.get("desable_test_session_status_update", False)
        if desable_test_session_status_update:
            return

        self.update_local_env_test_status(exitstatus == ExitCode.OK)
        status = True
        if "envIds" in self.conf:
            test_ok_count = 0
            for envId in self.conf["envIds"]:
                key = f"{self.mgr.get_case_extra()['productId']}/{self.mgr.get_testid()}/{envId}"
                test_ok = self.yj_data_get(key)
                if test_ok:
                    test_ok_count += 1
            # 所有环境都测试ok,才算一次完整的测试
            status = test_ok_count == len(self.conf["envIds"])

        self.update_status(status)
        if self.conf.get("report_manual_cases_info", False):
            self.yj_import_manual_cases(self.conf["manual_cases_file_path"])

    def yj_data_add(self, key, value):
        url = self.base_url + "/api/common/data-server/add"
        r = self.s.post(url, json={
            "key": key,
            "value": value,
        })
        assert r.status_code == 200

    def yj_data_get(self, key):
        url = self.base_url + "/api/common/data-server/get"
        r = self.s.get(url, params={
            "key": key,
        })
        assert r.status_code == 200
        return r.json()["data"]

    def yj_import_manual_cases(self, manual_test_case_infos_path):
        mh, md = read_dict_as_row(manual_test_case_infos_path)
        m2a = {
            "nodeid": "用例编号",
            "tags": "用例标签",
            "severity": "重要等级",
            "func_ids": "覆盖功能",
            "interface_ids": "覆盖接口",
            "test_result": "测试结果",
            "name": "用例名称",
            "desc": "备注",
            "issue": "议题",
            "suggest": "建议",
            "creator": "创建人员",
            "create_time": "创建时间",
        }
        for r in md:
            if not r["用例编号"]:
                print("空用例跳过")
                continue
            d = {"test_type": "manual"}
            for k in m2a.keys():
                try:
                    td = r[m2a[k]].replace("，", ",")
                    d[k] = td
                except KeyError:
                    logging.warning(f"表格中缺少'{m2a[k]}'列")
            d["create_time"] = "-".join(d["create_time"].split("/"))
            d["update_time"] = "-".join(d["create_time"].split("/"))
            d["duration"] = 1.2
            d["start_time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            d["begin_time"] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            d['test_type'] = 'manual'
            d['testid'] = self.mgr.get_testid()
            self.report_test_result(d)

    def update_function_ids(self, ids):
        url = self.base_url + "/api/data_collction/function/ids"
        r = self.s.post(url, json={
            "testId": self.mgr.get_testid(),
            "productId": self.productId,
            "ids": ids,
        })
        assert r.status_code == 200

    def update_interface_ids(self, ids):
        url = self.base_url + "/api/data_collction/interface/ids"
        r = self.s.post(url, json={
            "testId": self.mgr.get_testid(),
            "productId": self.productId,
            "ids": ids,
        })
        assert r.status_code == 200

    def update_status(self, status):
        url = self.base_url + "/api/data_collction/status"

        r = self.s.post(url, json={
            "testId": self.mgr.get_testid(),
            "productId": self.productId,
            "status": status,
        })
        assert r.status_code == 200

    def update_version_info(self, versionInfo):
        url = self.base_url + "/api/data_collction/version/info"
        r = self.s.post(url, json={
            "testId": self.mgr.get_testid(),
            "productId": self.productId,
            "versionInfo": versionInfo,
        })
        assert r.status_code == 200


Plugin = OlymYjOutEnd
