import os
from functools import wraps
from os import system

from flask import request, Flask, send_file, abort, render_template

from olymat.mgr import inc_testid
from olymat.utils.dir_helper import make_rel_path
from olymat.utils.log_helper import logi, update_log_level

olymat_webserver = Flask(__name__, template_folder=make_rel_path(__file__, "assists"))

# 存储服务状态，默认为启用
service_status = {"enabled": True}

UPLOAD_FOLDER = os.path.join(os.getcwd())
os.makedirs(UPLOAD_FOLDER, exist_ok=True)


# 测试服务是否启用的装饰器
def check_service_enabled(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # 检查服务状态
        if not service_status["enabled"]:
            return {
                "code": 1,
                "msg": "当前测试服务正忙，请稍候再试或联系管理员",
                "data": None
            }
        return func(*args, **kwargs)

    return wrapper


# 状态查询接口，用于查询服务的状态
@olymat_webserver.route('/api/get_status')
def get_status():
    return {
        "code": 0,
        "msg": "",
        "data": {
            "status": "enabled" if service_status["enabled"] else "disabled",
            "cwd": os.getcwd(),
        }
    }


# 状态设置接口，用于设置服务的状态
@olymat_webserver.route('/api/set_status')
def set_status():
    status = request.args.get('status')

    # 检查传入的状态是否合法
    if status not in ["enabled", "disabled"]:
        return {
            "code": 1,
            "msg": "Invalid status value",
            "data": None
        }

    # 设置服务状态
    service_status["enabled"] = (status == "enabled")

    # 返回结果
    return {
        "code": 0,
        "msg": "",
        "data": None
    }


# 上传文件接口
@olymat_webserver.route('/api/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return {
            "code": 1,
            "msg": "No file part in the request",
            "data": None
        }

    file = request.files['file']
    if file.filename == '':
        return {
            "code": 1,
            "msg": "No file selected for uploading",
            "data": None
        }

    target_path = request.form.get('target_path', '')
    if not target_path:
        return {
            "code": 1,
            "msg": "Target path not specified",
            "data": None
        }

    save_path = os.path.join(UPLOAD_FOLDER, target_path)
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    try:
        file.save(save_path)
    except Exception as e:
        return {
            "code": 1,
            "msg": f"File upload failed: {str(e)}",
            "data": None
        }

    return {
        "code": 0,
        "msg": "File uploaded successfully",
        "data": {
            "file_path": save_path
        }
    }


@olymat_webserver.route('/reports/', defaults={'req_path': ''})
@olymat_webserver.route('/reports/<path:req_path>')
def dir_listing(req_path):
    print(request.path, req_path)
    BASE_DIR = os.path.abspath(os.path.join(os.getcwd(), "reports"))

    # Joining the base and the requested path
    abs_path = os.path.join(BASE_DIR, req_path)

    # Return 404 if path doesn't exist
    if not os.path.exists(abs_path):
        return abort(404)

    # Check if path is a file and serve
    if os.path.isfile(abs_path):
        return send_file(abs_path)

    # Show directory contents
    files = os.listdir(abs_path)
    files = sorted(files, reverse=True)
    return render_template('files.html', files=files)


@olymat_webserver.route('/api/inc_testid')
def api_inc_testid():
    new_testid = inc_testid()
    return {
        "code": 0,
        "msg": "",
        "data": {
            "new_testid": new_testid
        }
    }


@olymat_webserver.route('/api/start_test')
@check_service_enabled
def api_start_test():
    cmd = request.args.get('testcases')

    f_cmd = "start cmd /k pytest " + cmd
    print(f_cmd)
    return {
        "code": 0,
        "msg": "",
        "data": system(f_cmd)
    }


@olymat_webserver.route('/api/start_test2')
@check_service_enabled
def api_start_test2():
    testcases = request.args.get('testcases')
    testcases = testcases.split(" ")[0]
    conf_name = request.args.get('conf_name')

    f_cmd = f"start cmd /k pytest --config={conf_name} " + testcases

    print(f_cmd)
    return {
        "code": 0,
        "msg": "",
        "data": system(f_cmd)
    }


def main():
    update_log_level()
    logi(f"当前工作目录 {os.getcwd()}")
    olymat_webserver.run(host="0.0.0.0", port=9999)


if __name__ == "__main__":
    main()
