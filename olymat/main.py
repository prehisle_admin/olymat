import argparse
import logging
import os
import shutil
import subprocess
from distutils.dir_util import copy_tree

from olymat.utils.windows_helper import WindowCmdWrap

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


def inc_testid_func(args):
    from olymat.mgr import inc_testid
    inc_testid()


def webserver_func(args):
    from olymat.webserver import olymat_webserver
    olymat_webserver.run(host=args.IP, port=args.PORT)


def get_python_path():
    result = subprocess.run(["where", "python"], capture_output=True, text=True)
    paths = result.stdout.splitlines()
    if paths:
        return paths[1] if len(paths) > 1 else None
    else:
        return None


def create_func(args):
    path = os.path.abspath(__file__)
    directory = os.path.dirname(path)
    prj_tpl_path = os.path.join(directory, "prj_tpl")
    shutil.copytree(prj_tpl_path, args.prj_name)
    shutil.copy(os.path.join(args.prj_name, "test_hello", "confs", "template.yaml"),
                os.path.join(args.prj_name, "test_hello", "confs", "default.yaml"))
    if args.init:
        cmd = WindowCmdWrap(["cmd", "/k"], shell=True)
        cmd.exec(f"""cd /d "{args.prj_name}" """)
        python_exe_path = get_python_path()
        if python_exe_path:
            cmd.exec(f"virtualenv -p {python_exe_path} venv")
        else:
            cmd.exec(f"virtualenv venv")
        cmd.exec(fr"venv\scripts\activate", wait_str=">")
        cmd.exec("set prompt=$lolym$g")
        cmd.exec(fr"pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple")
        cmd.exec(f"set PYTHONHOME=.")
        # cmd.exec(r'''python -c "import sys; print('\n'.join(sys.path))"''')
        cmd.exec(
            "pip install --force-reinstall -r requirements.txt")
        cmd.exec(
            "custom_deps.bat")
        cmd.exec(fr"venv\scripts\deactivate", wait_str=">")


def init_tpl_func(args):
    path = os.path.abspath(__file__)
    directory = os.path.dirname(path)
    tpl_path = os.path.join(directory, "prj_tpl", args.tpl_name)
    assert os.path.exists(tpl_path), f"模板目录不存在 {tpl_path}"
    copy_tree(tpl_path, os.getcwd())
    shutil.copy(os.path.join(os.getcwd(), "confs", "template.yaml"), os.path.join(os.getcwd(), "confs", "default.yaml"))


def main():
    parser = argparse.ArgumentParser(description='奥联自动化测试框架.')
    subparsers = parser.add_subparsers(help='要执行的目标指令', dest="cmd")
    subparsers.required = True

    webserver = subparsers.add_parser('webserver', help='启动测试服务器')
    webserver.add_argument('IP', nargs='?', type=str, help="服务器IP", default="0.0.0.0")
    webserver.add_argument('PORT', nargs='?', type=int, help="服务器PORT", default=9999)
    webserver.set_defaults(func=webserver_func)

    inc_testid = subparsers.add_parser('inc_testid', help='更新测试ID')
    inc_testid.set_defaults(func=inc_testid_func)

    create = subparsers.add_parser('create', help='创建项目')
    create.add_argument('prj_name', type=str, help='项目名称')
    create.add_argument('--init', action='store_true', default=False, help='将项目初始化')
    create.set_defaults(func=create_func)

    create = subparsers.add_parser('init_tpl', help='使用模板初始化当前目录')
    create.add_argument('--tpl_name', type=str, default="test_hello", help='模板名')
    create.set_defaults(func=init_tpl_func)

    args = parser.parse_args()
    try:
        args.func(args)
    except:
        logging.error("occur error", exc_info=True)


if __name__ == "__main__":
    main()
