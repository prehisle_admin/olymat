import io
import os
import sys

sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')
sys.stderr = io.TextIOWrapper(sys.stderr.buffer, encoding='utf-8')
import logging
from olymat.utils.windows_helper import WindowCmdWrap, WindowCmdWrap2

# from pip._internal.cli import main as pipmain

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s")
from olymat.utils.log_helper import logi

sys.path.append('.')

for v in filter(lambda v: "git" in v.lower(), os.environ["path"].split(";")):
    sys.path.append(v)


# sys.path.append(os.path.join(os.path.dirname(sys.executable), ))

# print(sys.executable)
# print(sys.argv)
# print(os.getcwd())
# print(__name__)


def upgrade(args):
    logi("执行更新操作")
    git_repos_url = args[0]
    other_args = args[1:]
    logi(f'''
版本库地址：{git_repos_url}
其他参数：{other_args}
工作目录: {os.getcwd()}
执行文件: {sys.executable}
''')
    branch_name = other_args[0] if len(other_args) > 0 else ""
    logi(f"分支 '{branch_name}'")
    # 兼容未设置branch的版本
    prj_name = os.path.basename(git_repos_url).split(".")[0] + "." + branch_name if branch_name else \
        os.path.basename(git_repos_url).split(".")[0]
    full_prj_path = os.path.join(os.getcwd(), prj_name)
    is_requirements_update = False
    is_custom_deps_update = False

    cmd = WindowCmdWrap2(["cmd", "/k"], shell=True)
    cmd.exec('taskkill /IM "olymat.exe" /F ')
    cmd.exec(fr"chcp 65001")
    cmd.exec(fr"cd /d {full_prj_path}")

    if os.path.isdir(full_prj_path):
        logi(f"目录{full_prj_path}存在")
        git_cmd = f"git pull"
        output = cmd.exec(git_cmd)
        is_requirements_update = "requirements" in output
        is_custom_deps_update = "custom_deps" in output
        if is_requirements_update or is_custom_deps_update: # 已经需要更新了
            pass
        elif "Already up to date." not in output: # 不需要再将判断
            output = cmd.exec("git log --name-only HEAD~1..HEAD")
            is_requirements_update = "requirements" in output
            is_custom_deps_update = "custom_deps" in output
    else:
        logi(f"目录{full_prj_path}不存在, git拉取")
        if branch_name:
            git_cmd = f"""git clone {git_repos_url} --progress --depth 1 -b {branch_name} "{full_prj_path}" """
        else:
            git_cmd = f"""git clone {git_repos_url} --progress --depth 1 "{full_prj_path}" """
        logi(f"执行拉取命令：{git_cmd}")
        sys.stdout.flush()
        cmd.exec(git_cmd)

    venv_dir = os.path.join(full_prj_path, "venv")
    if os.path.isdir(venv_dir):
        logi(f"venv目录{venv_dir}存在")
    else:
        logi(f"目录{venv_dir}不存在, 创建")
        venv_create_cmd = f"""virtualenv "{venv_dir}" """
        os.system(venv_create_cmd)
        is_requirements_update = True
        is_custom_deps_update = True
        cmd = WindowCmdWrap(["cmd", "/k"], shell=False)
        cmd.exec(fr"venv\scripts\activate && chcp 65001 && set prompt=$lolym$g")
        cmd.exec(r'''set PYTHONHOME=.''')  # 禁止到注册表中查找系统安装路径并应用, 进面引起库冲突
        cmd.exec(fr"pip config set global.trusted-host mirrors.aliyun.com")
        cmd.exec(fr"pip config set global.index-url http://mirrors.aliyun.com/pypi/simple")
        cmd.close()

    os.chdir(full_prj_path)
    if os.path.isfile(os.path.join(full_prj_path, "requirements.txt")) and is_requirements_update:
        logi("重装requirements.txt")
        cmd = WindowCmdWrap(["cmd", "/k"], shell=False)
        cmd.exec(fr"venv\scripts\activate && chcp 65001 && set prompt=$lolym$g")
        cmd.exec(r'''set PYTHONHOME=.''')  # 禁止到注册表中查找系统安装路径并应用, 进面引起库冲突
        cmd.exec(f"pip install --force-reinstall -r requirements.txt")
        cmd.close()

    custom_deps_file = os.path.join(full_prj_path, "custom_deps.bat")
    if is_custom_deps_update and os.path.isfile(custom_deps_file):
        from olymat.utils.file_helper import read_file
        cmd = WindowCmdWrap(["cmd", "/k"], shell=False)
        cmd.exec(fr"venv\scripts\activate && chcp 65001 && set prompt=$lolym$g")
        for line in read_file(custom_deps_file).decode("utf-8").split("\n"):
            cmd.exec(line)
        cmd.close()


def main():
    os.environ["PYTHONUNBUFFERED"] = "1"
    if sys.argv[1] == "--upgrade":
        upgrade(sys.argv[2:])
    else:
        logi("切换到venv")
        logi(f'''
        其他参数：{sys.argv[1:]}
        工作目录: {os.getcwd()}
        执行文件: {sys.executable}
        path: {os.environ["path"]}
        ''')
        # 直接使用原始参数，不进行分割
        args_str = ' '.join(f'"{arg}"' if ' ' in arg else arg for arg in sys.argv[1:])
        cmd = fr"venv\scripts\activate && pytest {args_str}"
        os.system(cmd)


if __name__ == "__main__":
    main()

# python olymat\olymat_cli.py --upgrade https://gitee.com/prehisle_admin/olymat-tools.git
