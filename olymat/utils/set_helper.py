class LimitedSet(set):
    def __init__(self, max_size):
        super().__init__()
        self.max_size = max_size

    def add(self, key):
        if len(self) >= self.max_size:
            self.remove(next(iter(self)))
        super().add(key)

    def check_and_add(self, key):
        if key not in self:
            self.add(key)
            return True
        return False