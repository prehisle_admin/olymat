import collections
import os
import zipfile


def update(d, u):
    '''
    合并字典，包括子项
    :param d:
    :param u:
    :return:
    '''
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


class DictObj:
    '''
    用.访问dict对象
    '''

    def __init__(self, in_dict: dict):
        assert isinstance(in_dict, dict)
        for key, val in in_dict.items():
            if isinstance(val, (list, tuple)):
                setattr(self, key, [DictObj(x) if isinstance(x, dict) else x for x in val])
            else:
                setattr(self, key, DictObj(val) if isinstance(val, dict) else val)


def func_run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.ret:
            wrapper.ret = f(*args, **kwargs)
        return wrapper.ret

    wrapper.ret = None
    return wrapper


def unzip_file(zip_filepath, extract_to_dir):
    """
    解压指定的zip文件到指定目录。

    :param zip_filepath: zip文件的路径
    :param extract_to_dir: 解压到的目标目录
    """
    # 检查目标目录是否存在，如果不存在则创建
    if not os.path.exists(extract_to_dir):
        os.makedirs(extract_to_dir)

    # 打开zip文件
    with zipfile.ZipFile(zip_filepath, 'r') as zip_ref:
        # 解压所有内容到目标目录
        zip_ref.extractall(extract_to_dir)
        print(f"文件已解压到 {extract_to_dir}")
