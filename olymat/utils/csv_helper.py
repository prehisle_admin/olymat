import csv
import os

from olymat.utils.file_helper import touch


def append_row(file_name, row_data, encoding="utf-8"):
    with open(file_name, 'a+', newline='', encoding=encoding) as f:
        csv.writer(f).writerow(row_data)


def append_rows(file_name, row_data, encoding="utf-8"):
    with open(file_name, 'a+', newline='', encoding=encoding) as f:
        csv.writer(f).writerows(row_data)


def read_dict_as_row(file_name, encoding="utf-8"):
    with open(file_name, 'r', newline='', encoding=encoding) as r_obj:
        r = csv.DictReader(r_obj)
        return r.fieldnames, list(r)


def read_csv(file_name, encoding="utf-8"):
    with open(file_name, 'r', newline='', encoding=encoding) as r_obj:
        r = csv.reader(r_obj)
        return list(r)


def read_csv_header_and_data(file_name, encoding="utf-8"):
    data = read_csv(file_name, encoding=encoding)
    return data[0], data[1:]


def append_dict_as_row(file_name, dict_of_elem, field_names, encoding="utf-8"):
    # Open file in append mode
    exists = os.path.exists(file_name)
    if not exists:
        touch(file_name)
    with open(file_name, 'a+', newline='', encoding=encoding) as write_obj:
        # Create a writer object from csv module
        dict_writer = csv.DictWriter(write_obj, fieldnames=field_names)
        if not exists:
            dict_writer.writeheader()
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)


def append_dict_as_row_simple(file_name, dict_of_elem, encoding="utf-8"):
    exists = os.path.exists(file_name)
    if not exists:
        touch(file_name)
    with open(file_name, 'a+', newline='', encoding=encoding) as write_obj:
        dict_writer = csv.DictWriter(write_obj, fieldnames=dict_of_elem.keys())
        if not exists:
            dict_writer.writeheader()
        dict_writer.writerow(dict_of_elem)


class CSVTool:
    def __init__(self, file_path, encoding="utf-8"):
        self.encoding = encoding
        self.file_path = file_path
        self.header = None

    def reset(self):
        """重置CSV文件（清空内容）"""
        open(self.file_path, 'w').close()
        self.header = None
        return self

    def set_header(self, header):
        """设置CSV文件的表头"""
        with open(self.file_path, 'w', newline='', encoding=self.encoding) as file:
            writer = csv.writer(file)
            writer.writerow(header)
        self.header = header
        return self

    def add_row(self, row_data):
        """向CSV文件增加一行数据"""
        if not self.header:
            raise Exception("Header not set. Please set the header before adding rows.")

        with open(self.file_path, 'a', newline='', encoding=self.encoding) as file:
            writer = csv.writer(file)
            writer.writerow(row_data)
        return self

    def add_row_from_dict(self, row_dict):
        """用dict方式写入一行数据"""
        if not self.header:
            raise Exception("Header not set. Please set the header before adding rows.")

        # 确保字典中的键与表头匹配，未提供的列填入空值
        row_data = [row_dict.get(col, "") for col in self.header]

        with open(self.file_path, 'a', newline='', encoding=self.encoding) as file:
            writer = csv.writer(file)
            writer.writerow(row_data)
        return self

    def read_all_rows(self):
        """读取所有的行"""
        with open(self.file_path, 'r', newline='', encoding=self.encoding) as file:
            reader = csv.reader(file)
            rows = list(reader)
        return rows

    def read_as_dicts(self):
        """读取所有行记录与表头组成的dict记录列表"""
        with open(self.file_path, 'r', newline='', encoding=self.encoding) as file:
            reader = csv.DictReader(file)
            dict_list = [row for row in reader]
        return dict_list

    def get_headers(self):
        try:
            with open(self.file_path, 'r', newline='', encoding=self.encoding) as file:
                reader = csv.reader(file)
                headers = next(reader, [])  # 获取第一行作为表头
            return headers
        except FileNotFoundError:
            print(f"文件未找到: {self.file_path}")
            return []
        except Exception as e:
            print(f"读取表头时发生错误: {e}")
            return []
