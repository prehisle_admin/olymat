import logging
import platform
import subprocess

from olymat.utils.control_helper import call_until_success, RetryException

if platform.system().lower() == "windows":
    def is_ping_ok(host):
        code, output = subprocess.getstatusoutput("ping -n 1 -w 1 %s" % host)
        logging.info("is_ping_ok output[%s]" % output)
        return "TTL=" in output
else:
    def is_ping_ok(host):
        cmd = f"ping -c 1 -W 1 {host}"
        code, output = subprocess.getstatusoutput(cmd)
        logging.info("is_ping_ok output[%s]", output)
        return "ttl=" in output


@call_until_success(try_times=200, sleep_time=2)
def wait_ping_ok(host):
    if not is_ping_ok(host):
        raise RetryException()


@call_until_success(try_times=200, sleep_time=2)
def wait_ping_fail(host):
    if is_ping_ok(host):
        raise RetryException()
