import string

ALPHABET_64 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
ALPHABET_26 = "abcdefghijklmnopqrstuvwxyz"
S_DIGITS = string.digits
S_LOWER_LETTER = string.ascii_lowercase
S_DIGITS_AND_LOWER_LETTER = S_DIGITS + S_LOWER_LETTER


def basex_encode(num, alphabet=ALPHABET_64, fix_width=0):
    """Encode a number in Base X

    `num`: The number to encode
    `alphabet`: The alphabet to use for encoding
    """
    if (num == 0):
        return alphabet[0]
    arr = []
    base = len(alphabet)
    while num:
        rem = num % base
        num = num // base
        arr.append(alphabet[rem])
    arr.reverse()
    return ''.join(arr).rjust(fix_width, alphabet[0])[-fix_width:]


def basex_decode(string, alphabet=ALPHABET_64):
    """Decode a Base X encoded string into the number

    Arguments:
    - `string`: The encoded string
    - `alphabet`: The alphabet to use for encoding
    """
    base = len(alphabet)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1

    return num


def letter_name_add(name, len=3):
    return basex_encode(basex_decode(name, ALPHABET_26) + 1, ALPHABET_26, len)


def string_add(start_str, string_seed, len=0):
    return basex_encode(basex_decode(start_str, string_seed) + 1, string_seed, len)


def name_add(name):
    name = string_add(name, S_DIGITS_AND_LOWER_LETTER)
    if name[0] in S_DIGITS:
        name = 'a' + name[1:]
    return name
