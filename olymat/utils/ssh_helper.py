import glob
import io
import logging
import os
import posixpath
import re
import socket
import stat
import sys
import tempfile
import textwrap
import time

import paramiko as paramiko
import pytest
from paramiko.buffered_pipe import PipeTimeout

from olymat.utils.control_helper import call_until_success, RetryException, RetryOvertimesException, TimeoutException
from olymat.utils.dict_helper import merge_dicts_deep, DotDict
from olymat.utils.file_helper import read_file, write_file
from olymat.utils.log_helper import logi

logging.getLogger("paramiko").setLevel(logging.CRITICAL)
from scp import SCPClient


# paramiko.util.log_to_file("paramiko.log")
@call_until_success(
    retry_exceptions=[paramiko.ssh_exception.NoValidConnectionsError, TimeoutError, EOFError, socket.timeout,
                      paramiko.ssh_exception.SSHException],
    try_times=60, sleep_time=5)
def ssh_login(ip, port, username, password=None, key_file=None, key_content=None):
    """
    登录到远程服务器。

    :param ip: 远程服务器的IP地址
    :param port: SSH端口号
    :param username: 用户名
    :param password: 密码（可选）
    :param key_file: 私钥文件路径（可选）
    :param key_content: 私钥字符串内容（可选）
    :return: paramiko.SSHClient 实例
    """
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    try:
        if key_file:
            # 使用密钥文件进行认证
            private_key = paramiko.RSAKey.from_private_key_file(key_file)
            # print(f"Connecting using key file {key_file}")
            ssh.connect(ip, port, username=username, pkey=private_key, timeout=5, allow_agent=False,
                        look_for_keys=False)
        elif key_content:
            # 使用密钥字符串内容进行认证
            private_key = paramiko.RSAKey.from_private_key(io.StringIO(key_content))
            # print("Connecting using key content")
            ssh.connect(ip, port, username=username, pkey=private_key, timeout=5, allow_agent=False,
                        look_for_keys=False)
        elif password:
            # 使用密码进行认证
            # print(f"Connecting using password")
            ssh.connect(ip, port, username=username, password=password, timeout=5, allow_agent=False,
                        look_for_keys=False)
        else:
            raise ValueError("Either password, key_file, or key_content must be provided for authentication.")

        # print("Connection successful")
        return ssh

    except Exception as e:
        # print(f"SSH connection failed: {e}")
        ssh.close()
        raise


def ssh_exec(ssh, cmd, encoding="utf-8"):
    stdin, stdout, stderr = ssh.exec_command(cmd)
    exit_code = stdout.channel.recv_exit_status()
    return exit_code, (stdout.read() + stderr.read()).decode(encoding, errors='replace')


def ssh_exec_sudo(ssh, cmd, password, encoding="utf-8"):
    stdin, stdout, stderr = ssh.exec_command("sudo -S -p '' %s" % cmd)
    stdin.write(password + "\n")
    stdin.flush()
    stderr_str = stderr.read().decode(encoding, errors='replace')
    stdout_str = stdout.read().decode(encoding, errors='replace')
    exit_code = stdout.channel.recv_exit_status()
    logging.info("stderr: %s" % stderr_str)
    logging.info("stdout: %s" % stdout_str)
    logging.info("exit_code: %d" % exit_code)
    return exit_code, stdout_str, stderr_str


def ssh_upload_file(ssh, local_file_path, remote_file_path):
    ftp_client = ssh.open_sftp()
    ftp_client.put(local_file_path, remote_file_path)
    ftp_client.chmod(remote_file_path, os.lstat(local_file_path).st_mode)
    ftp_client.close()


def ssh_download_file(ssh, remote_file_path, local_file_path):
    ftp_client = ssh.open_sftp()
    ftp_client.get(remote_file_path, local_file_path)
    ftp_client.close()


def wait_for_multi_str(chan, strs):
    strs = [strs] if type(strs) == str else list(map(lambda x: x, strs))
    rs = ""
    while True:
        try:
            r = chan.recv(9999).decode("utf-8", errors="ignore")
            print(r, end="", flush=True)
            rs += r
        except TimeoutError:
            raise Exception(f"wait {strs} timeout {chan.timeout}")
        for s in strs:
            if s in r:
                return s, rs


def wait_for_multi_str2(chan, wait_str_bytes):
    wait_str_bytes = [wait_str_bytes.encode("utf-8")] if type(wait_str_bytes) == str else \
        list(map(lambda x: x.encode("utf-8"), wait_str_bytes))
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            r = r.replace(b"\r", b"")
            sys.stdout.buffer.write(r)
            rs += r
        except TimeoutError:
            raise Exception(f"wait {str(wait_str_bytes)} timeout {chan.timeout}")

        for s in wait_str_bytes:
            if s in r[-len(s) - 10:]:
                return rs.decode("utf-8", errors="ignore")


def wait_for_multi_str3(chan, wait_str_bytes):
    wait_str_bytes = [wait_str_bytes.encode("utf-8")] if type(wait_str_bytes) == str else \
        list(map(lambda x: x.encode("utf-8"), wait_str_bytes))
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            # r = ansi_escape.sub(b'', r)
            sys.stdout.buffer.write(r)
            sys.stdout.buffer.flush()
            rs += r
        except TimeoutError:
            raise Exception(f"wait {str(wait_str_bytes)} timeout {chan.timeout}")

        for s in wait_str_bytes:
            if s in r[-len(s) - 10:]:
                return rs.decode("utf-8", errors="ignore")


def wait_for_multi_str4(chan, wait_str, times=1):
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            sys.stdout.buffer.write(r)
            sys.stdout.buffer.flush()
            rs += r
        except TimeoutError:
            raise Exception(f"wait {str(wait_str)} timeout {chan.timeout}")

        rss = rs.decode("utf-8", errors="ignore")
        if len(re.findall(wait_str, rss)) >= times:
            return rss


def wait_for_multi_str5(chan, wait_str, times=1, run_timeout=600):
    start_time = time.time()
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            # sys.stdout.buffer.write(r)
            # sys.stdout.buffer.flush()
            print(r.decode(errors='replace'), end="", flush=True)
            rs += r
            if run_timeout and time.time() - start_time > run_timeout:
                raise Exception(f"{str(wait_str)} run_timeout {run_timeout}")
        except TimeoutError:
            raise Exception(f"wait {str(wait_str)} timeout {chan.timeout}")

        if len(re.findall(wait_str, rs)) >= times:
            return rs.decode(errors='replace')


def wait_for_multi_str6(chan, wait_str, times=1):
    rs = b""
    wait_strs = [wait_str] if type(wait_str) == "str" else wait_str
    while True:
        try:
            r = chan.recv(9999)
            sys.stdout.buffer.write(r)
            sys.stdout.buffer.flush()
            rs += r
        except TimeoutError:
            raise Exception(f"wait {str(wait_str)} timeout {chan.timeout}")

        find_times = 0
        for ws in wait_strs:
            find_times += len(re.findall(ws, rs))
        if find_times >= times:
            return rs.decode(errors='replace')


def wait_for_multi_str7(chan, wait_str, times=1, run_timeout=600, re_match=False):
    start_time = time.time()
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            # sys.stdout.buffer.write(r)
            # sys.stdout.buffer.flush()
            print(r.decode(errors='replace'), end="", flush=True)
            rs += r
            if run_timeout and time.time() - start_time > run_timeout:
                raise TimeoutException(f"{str(wait_str)} run_timeout {run_timeout}")
        except TimeoutError:
            raise TimeoutException(f"wait {str(wait_str)} timeout {chan.timeout}")

        if re_match:
            if len(re.findall(wait_str, rs)) >= times:
                return rs.decode(errors='replace')
        else:
            if rs.count(wait_str) >= times:
                return rs.decode(errors='replace')


def wait_for_multi_str8(chan, wait_str, times=1, run_timeout=600, re_match=False, re_flags=0, max_return_size=None):
    if isinstance(wait_str, str):
        wait_str = wait_str.encode()
    start_time = time.time()
    rs = b""
    while True:
        try:
            r = chan.recv(9999)
            # sys.stdout.buffer.write(r)
            # sys.stdout.buffer.flush()
            print(r.decode(errors='replace'), end="", flush=True)
            rs += r
            if max_return_size and len(rs) > max_return_size:
                rs = rs[len(rs) - max_return_size:]
            if run_timeout and time.time() - start_time > run_timeout:
                raise TimeoutException(f"{str(wait_str.decode())} run_timeout {run_timeout}")
        except TimeoutError:
            raise TimeoutException(f"wait {str(wait_str.decode())} timeout {chan.timeout}")

        if re_match:
            if len(re.findall(wait_str, rs, flags=re_flags)) >= times:
                return rs.decode(errors='replace')
        else:
            if rs.count(wait_str) >= times:
                return rs.decode(errors='replace')


class SshHelper:
    # TERM = "xterm-256color"
    TERM = "vt100"  # 兼容性更好
    WIDTH = 192  # 窗口能显示的字符数小于这个宽度时,  docker-compose up 效果会有问题, 大于则ok
    HEIGHT = 24

    def __init__(self, ip, port=22, username="", password="", encoding="", is_windows=False, init_wait_str=b"# ",
                 ctx=None,
                 **kwargs):
        # kwargs 用于忽略多转入的参数
        self.is_windows = is_windows
        self.encoding = "gbk" if self.is_windows else "utf-8"
        if encoding:
            self.encoding = encoding
        self.password = password
        self.ssh = None
        self.chan = None
        self.until_str = init_wait_str
        self.ctx = ctx
        self.ip = ip
        self.username = username
        self.password = password
        self.recv_buff_size = 1
        key_file = kwargs.get('key_file', None)
        key_content = kwargs.get('key_content', None)

        self.ssh = ssh_login(ip,
                             port,
                             username,
                             password,
                             key_file=key_file,
                             key_content=key_content)

    def __del__(self):
        self.close()

    def close(self):
        if self.chan:
            try:
                self.chan.close()
            except EOFError:
                pass
        if self.ssh:
            self.ssh.close()

    def set_ctx(self, ctx):
        self.ctx = ctx

    @call_until_success(try_times=60, sleep_time=5, retry_exceptions=[PipeTimeout, ])
    def start_chan(self, timeout=2, prompt=None, su=True, width=1024, first_new_line=True, **kwargs):
        if self.chan:
            self.chan.close()
        if first_new_line:
            print("\n")
        self.chan = self.ssh.invoke_shell(term=SshHelper.TERM, width=SshHelper.WIDTH)
        # self.chan = self.ssh.invoke_shell(self.TERM)
        # 关闭标准错误流的读取，将错误合并到标准输出
        self.chan.set_combine_stderr(True)
        self.chan.settimeout(timeout)
        # self.ssh.get_transport().clear_to_send_timeout = 60
        # self.ssh.get_transport().set_keepalive(60)
        seed_prompts = [":~", "]# ", ":~$", "~]$", "$ "]
        if prompt:
            seed_prompts.append(prompt)
        m, r = wait_for_multi_str(self.chan, seed_prompts)
        if su and (r.endswith(":~") or r.endswith(":~$ ") or r.endswith("~]$ ") or r.endswith("$ ")):
            self.chan_exec("sudo -i", [":", "："])  # [sudo] password for olym:
            self.chan_exec(self.password, [":~# ", "~]# "])

    @call_until_success(try_times=60, sleep_time=5, retry_exceptions=[PipeTimeout, ])
    def start_chan2(self, su_password, timeout=600, seed_prompts=["~ $ "], su=True):
        if self.chan:
            self.chan.close()
        # self.chan = self.ssh.invoke_shell(width=1024)  # 用这行会导致xterm.js,docker-compose up -d这种擦除效果混乱
        self.chan = self.ssh.invoke_shell()
        self.chan.settimeout(timeout)
        m, r = wait_for_multi_str(self.chan, seed_prompts)
        if su:
            self.chan_exec("su", ":")
            self.chan_exec(su_password, [" # "])

    @call_until_success(try_times=60, sleep_time=5, retry_exceptions=[PipeTimeout, ])
    def start_chan3(self, width=80, timeout=600):
        if self.chan:
            self.chan.close()
        self.chan = self.ssh.invoke_shell(width=width)
        # 关闭标准错误流的读取，将错误合并到标准输出
        self.chan.set_combine_stderr(True)
        self.chan.settimeout(timeout)

    def chan_exec(self, cmd, until_str="# ", timeout=600, exec_sleep=0):
        # logging.info(f"cmd:{cmd}, until_str:{until_str}")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str(self.chan, until_str)
        # logging.info(f"return:{data}")
        return r

    def chan_exec2(self, cmd, until_str="# ", timeout=600, exec_sleep=0):
        # logging.info(f"cmd:{cmd}, until_str:{until_str}")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str2(self.chan, until_str)
        # logging.info(f"return:{data}")
        return r

    def chan_exec3(self, cmd, until_str="# ", until_str_times=1, timeout=600, exec_sleep=0):
        # logging.info(f"cmd:{cmd}, until_str:{until_str}")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        for _ in range(until_str_times):
            r = wait_for_multi_str3(self.chan, until_str)
        # logging.info(f"return:{data}")
        print()
        return r

    def chan_exec4(self, cmd, until_str="# ", wait_str_times=1, timeout=600, exec_sleep=0):
        # logging.info(f"cmd:{cmd}, until_str:{until_str}")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str4(self.chan, until_str, wait_str_times)
        # logging.info(f"return:{data}")
        return r

    def exec_chan(self, cmd, until_str="# ", wait_str_times=1, timeout=600, exec_sleep=0):
        # logging.info(f"cmd:{cmd}, until_str:{until_str}")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str4(self.chan, until_str, wait_str_times)
        # logging.info(f"return:{data}")
        print()
        return r

    def update_wait_str(self, until_str):
        self.until_str = until_str

    def update_timeout(self, timeout):
        self.chan.settimeout(timeout)

    def update_recv_buff_size(self, recv_buff_size):
        self.recv_buff_size = recv_buff_size

    def exec_chan5(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600):
        '''
        1. 结合update_wait_str使用，避免每次都输入until_str参数
        2. 注意until_str类型为bytes，正则
        centos7 默认命令行提示符为b"]# "
        '''
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str5(self.chan, until_str if until_str else self.until_str, wait_str_times, run_timeout)
        return r

    def exec_chan7(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600, **kwargs):
        '''
        1. 结合update_wait_str使用，避免每次都输入until_str参数
        2. 注意until_str类型为bytes，正则
        centos7 默认命令行提示符为b"]# "
        '''
        if isinstance(until_str, str):
            until_str = until_str.encode("utf-8")
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str7(self.chan, until_str if until_str else self.until_str, wait_str_times, run_timeout,
                                **kwargs)
        return r

    def exec_chan8(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600,
                   auto_wait=False, print_new_line=False, **kwargs):
        if isinstance(until_str, str):
            until_str = until_str.encode("utf-8")
        if print_new_line:
            print("\n")

        self.chan.settimeout(timeout)
        if auto_wait:
            until_str = b'olymat_auto_wait_suffix'
            cmd = cmd + "\n echo 'olymat_auto_wait_suffix'"
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str8(self.chan, until_str if until_str else self.until_str, wait_str_times, run_timeout,
                                **kwargs)
        if print_new_line:
            print("\n")
        return r

    def exec_chan8_multi(self, multiline_cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0,
                         run_timeout=600,
                         auto_wait=False, **kwargs):
        multiline_cmd = textwrap.dedent(multiline_cmd)
        for cmd in multiline_cmd.strip().splitlines():
            self.exec_chan8(cmd, until_str=until_str, wait_str_times=wait_str_times, timeout=timeout,
                            exec_sleep=exec_sleep, run_timeout=run_timeout, auto_wait=auto_wait, **kwargs)

    def exec_chan9(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600, **kwargs):
        if isinstance(until_str, str):
            until_str = until_str.encode("utf-8")
        self.chan.settimeout(timeout)
        self.chan.send('%s' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str8(self.chan, until_str if until_str else self.until_str, wait_str_times, run_timeout,
                                **kwargs)
        return r

    def exec_chan7_ex(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600,
                      except_ignore=False, **kwargs):
        if except_ignore:
            try:
                self.exec_chan7(cmd, until_str=until_str, wait_str_times=wait_str_times, timeout=timeout,
                                exec_sleep=exec_sleep, run_timeout=run_timeout, **kwargs)
            except:
                pass
        else:
            self.exec_chan7(cmd, until_str=until_str, wait_str_times=wait_str_times, timeout=timeout,
                            exec_sleep=exec_sleep, run_timeout=run_timeout, **kwargs)

    def exec_chan5_ex(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0, run_timeout=600,
                      except_ignore=False):
        if except_ignore:
            try:
                self.exec_chan5(cmd, until_str=until_str, wait_str_times=wait_str_times, timeout=timeout,
                                exec_sleep=exec_sleep, run_timeout=run_timeout)
            except:
                pass
        else:
            self.exec_chan5(cmd, until_str=until_str, wait_str_times=wait_str_times, timeout=timeout,
                            exec_sleep=exec_sleep, run_timeout=run_timeout)

    def exec_chan_ex(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0):
        self.chan.settimeout(timeout)
        if type(until_str) == str:
            until_str = until_str.encode()
        self.chan.send('%s' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str5(self.chan, until_str if until_str else self.until_str, wait_str_times)
        return r

    def exec_chan6(self, cmd, until_str=b"", wait_str_times=1, timeout=600, exec_sleep=0):
        self.chan.settimeout(timeout)
        if self.is_windows:
            self.chan.send('%s\r\n' % cmd)
        else:
            self.chan.send('%s\n' % cmd)
        if exec_sleep > 0:
            time.sleep(exec_sleep)
        r = wait_for_multi_str6(self.chan, until_str if until_str else self.until_str, wait_str_times)
        return r

    def chan_exec_is_include(self, cmd, instr, until_str="# ", timeout=600):
        _, out = self.chan_exec(cmd, until_str, timeout)
        return instr in out

    @call_until_success(try_times=30, sleep_time=5)
    def chan_wait_until_cmd_ok(self, cmd, until_str):
        try:
            r = self.chan_exec(cmd, until_str)
            return r
        except OSError:
            raise RetryException()

    def exec_sudo(self, cmd):
        return self.exec_sudo2(cmd)[1]

    def exec_sudo2(self, cmd):
        stdin, stdout, stderr = self.ssh.exec_command(
            "sudo -S -p '' %s" % cmd)
        stdin.write(self.password + "\n")
        stdin.flush()
        exit_code = stdout.channel.recv_exit_status()
        return exit_code, (stdout.read() + stderr.read()).decode("utf-8", errors='replace')

    def exec(self, cmd):
        return self.exec2(cmd)[1]

    def exec_sudo_ex(self, cmd):
        return ssh_exec_sudo(self.ssh, cmd, self.password)

    @call_until_success(try_times=30, sleep_time=5)
    def wait_exec_ok(self, cmd, untils_str=""):
        try:
            r = self.exec(cmd)
        except OSError:
            raise RetryException()
        if untils_str and untils_str not in r:
            raise RetryException(untils_str)
        return r

    def wait_grep_exec_ok(self, cmd, untils_str):
        return self.wait_exec_ok(f'{cmd} | grep -v "grep" | grep --colour=never {untils_str}', untils_str)

    def exec2(self, cmd):
        # logging.info(f"cmd[{cmd}]")
        r = ssh_exec(self.ssh, cmd, self.encoding)
        # logging.info(f"output[{r}]")
        return r

    def exec_ex(self, cmd):
        code, output = ssh_exec(self.ssh, cmd, self.encoding)
        return code, output

    def exec_with_shell(self, cmd):
        return self.exec_with_shell2(cmd)[1]

    def exec_with_shell2(self, cmd):
        stdin, stdout, stderr = self.ssh.exec_command(cmd, get_pty=True)
        exit_code = stdout.channel.recv_exit_status()
        return exit_code, (stdout.read() + stderr.read()).decode(self.encoding, errors='replace')

    def sftp_put(self, local_file_path, remote_file_path):
        return ssh_upload_file(self.ssh, local_file_path, remote_file_path)

    def sftp_get(self, remote_file_path, local_file_path):
        return ssh_download_file(self.ssh, remote_file_path, local_file_path)

    def scp_put(self, local_file_path, remote_file_path):
        logi(f"scp_put {local_file_path} -> {remote_file_path}")
        with SCPClient(self.ssh.get_transport(), socket_timeout=120) as scp:
            scp.put(local_file_path, remote_file_path)

    def scp_get(self, remote_file_path, local_file_path):
        logi(f"scp_get {remote_file_path} -> {local_file_path}")
        local_file_path = os.path.abspath(local_file_path)
        if not os.path.isdir(os.path.dirname(local_file_path)):
            os.makedirs(os.path.dirname(local_file_path), exist_ok=True)
        with SCPClient(self.ssh.get_transport(), socket_timeout=120) as scp:
            scp.get(remote_file_path, local_file_path)

    @call_until_success(try_times=20, sleep_time=5)
    def wait_exec_ok_by_exitcode(self, cmd):
        exit_code, _ = ssh_exec(self.ssh, cmd)
        if exit_code != 0:
            raise RetryException()

    def exec_must_ok(self, cmd):
        exit_code, out = ssh_exec(self.ssh, cmd)
        assert exit_code == 0, f"{cmd} return code={exit_code}, output={out}"

    def is_file_exist(self, file_path):
        cmd = f'test -f {file_path}'
        code, out = self.exec2(cmd)
        return code == 0

    def is_dir_exist(self, dir_path):
        cmd = f'test -d {dir_path}'
        code, out = self.exec2(cmd)
        return code == 0

    def delete_file(self, path):
        self.exec(f"rm -rf {path}")
        assert not self.is_file_exist(path)

    def write_str_to_file(self, file_content, file_path):
        command = f"""\
cat << EOL > {file_path}
{file_content}
EOL"""
        self.exec_chan(command)

    def write_str_to_file2(self, file_path, file_content, until_str="# "):
        command = f"""\
tee {file_path} <<-'EOF'
{file_content}
EOF"""
        self.exec_chan(command, until_str=until_str)

    def append_str_to_file(self, file_path, file_content):
        command = f"""\
cat >> {file_path} << EOF
{file_content}
EOF"""
        self.exec_chan(command)

    def replace_str_to_file(self, file_path, sstr, dstr):
        raw_content = self.exec_sudo(f"cat {file_path}")
        dst_content = raw_content.replace(sstr, dstr)
        self.write_str_to_file2(file_path, dst_content)

    def replace_str_to_file_use_cat(self, file_path, sstr, dstr):
        raw_content = self.exec_sudo(f"cat {file_path}")
        dst_content = raw_content.replace(sstr, dstr)
        self.write_str_to_file(file_path, dst_content)

    def replace_str_to_file_use_scp(self, file_path, sstr, dstr):
        tmpfile_path = tempfile.NamedTemporaryFile(delete=False).name
        self.scp_get(file_path, tmpfile_path)
        os.chmod(tmpfile_path, stat.S_IWRITE)
        raw_content = read_file(tmpfile_path).decode()
        dst_content = raw_content.replace(sstr, dstr)
        write_file(tmpfile_path, dst_content.encode())
        self.scp_put(tmpfile_path, file_path)
        os.remove(tmpfile_path)

    def replace_file_str_re(self, file_path, pattern, repl, flags=0):
        tmpfile_path = tempfile.NamedTemporaryFile(delete=False).name
        self.scp_get(file_path, tmpfile_path)
        os.chmod(tmpfile_path, stat.S_IWRITE)
        raw_content = read_file(tmpfile_path).decode()
        dst_content = re.sub(pattern, repl, raw_content, flags=flags)
        write_file(tmpfile_path, dst_content.encode())
        self.scp_put(tmpfile_path, file_path)
        os.remove(tmpfile_path)

    def put_file_from_text(self, remote_file_path, file_content):
        tmpfile_path = tempfile.NamedTemporaryFile(delete=False).name
        write_file(tmpfile_path, file_content.encode())
        self.scp_put(tmpfile_path, remote_file_path)
        os.remove(tmpfile_path)

    def reset_dir_sudo(self, path):
        self.exec_sudo(f"rm -rf {path}")
        assert not self.is_dir_exist(path)
        self.exec_sudo(f"mkdir -p {path}")
        assert self.is_dir_exist(path)

    def reset_dir(self, path):
        self.exec(f"rm -rf {path}")
        assert not self.is_dir_exist(path)
        self.exec(f"mkdir -p {path}")
        assert self.is_dir_exist(path)

    def delete_file_sudo(self, path):
        self.exec_sudo(f"rm -rf {path}")
        assert not self.is_file_exist(path)

    def delete_file(self, path):
        self.exec(f"rm -rf {path}")
        assert not self.is_file_exist(path)

    def raw_send(self, data):
        if isinstance(data, str):
            if not data.endswith("\n"):
                data = data.encode() + b"\n"
            else:
                data = data.encode()
        self.chan.send(data)

    def raw_except(self, wait_msg=None, timeout=30, is_in=True, is_check_end=False, recv_buff_size=None):
        if not recv_buff_size:
            recv_buff_size = self.recv_buff_size
        if not wait_msg:
            wait_msg = self.until_str
        if isinstance(wait_msg, str):
            wait_msg = wait_msg.encode()
        self.chan.settimeout(timeout)
        start_time = time.time()
        if is_in:
            wait_msg = wait_msg if isinstance(wait_msg, list) else [wait_msg]
            wait_msg = [m.encode() if isinstance(m, str) else m for m in wait_msg]
        else:
            assert isinstance(wait_msg, bytes), "判断包含时, 只能是bytes"
        ret = b""
        while True:
            try:
                r = self.chan.recv(recv_buff_size)
                if time.time() - start_time > timeout:
                    raise TimeoutException(f"running timeout {timeout}")
            except TimeoutError:
                raise TimeoutException(f"waiting timeout {timeout}")
            sys.stdout.buffer.write(r)  # 直接输出字节字符串
            sys.stdout.flush()  # 刷新输出缓冲区
            ret += r
            if is_in:
                if all(m in ret for m in wait_msg):
                    if is_check_end:
                        if ret.endswith(wait_msg[-1]):  # 匹配最后一个字符串结尾
                            break
                    else:
                        break
            else:
                if ret.endswith(wait_msg):
                    break

        return ret

    def raw_send_wait(self, data, wait_msg=None, *args, **kwargs):
        self.raw_send(data)
        return self.raw_except(wait_msg=wait_msg, *args, **kwargs)

    def raw_send_wait_multi(self, lines, wait_msg=None, *args, **kwargs):
        for line in lines.strip().splitlines():
            self.raw_send_wait(line, wait_msg=wait_msg, *args, **kwargs)

    def _recv_data_until_match(self, match_func, timeout=30):
        """
        Generic method to receive data until a match is found.

        Args:
            match_func (callable): A function that checks for a match. It should accept
                                   the accumulated data (as string) and return the matching pattern or None.
            timeout (int): Timeout for the channel. Default is 30 seconds.

        Returns:
            tuple: The received data and the matching pattern (decoded to string if applicable).
        """
        self.chan.settimeout(timeout)

        ret = b""

        while True:
            r = self.chan.recv(9999)
            sys.stdout.buffer.write(r)  # Directly output the byte string
            sys.stdout.flush()  # Flush the output buffer
            ret += r

            match = match_func(ret)
            if match:
                return ret, match

    def wait_for_single_byte(self, byte_pattern, timeout=30):
        """
        Waits for a single byte pattern to be matched.

        Args:
            byte_pattern (bytes): The byte pattern to wait for.
            timeout (int): Timeout for the channel. Default is 30 seconds.

        Returns:
            tuple: The received data and the matching byte pattern.
        """

        def match_func(ret):
            return byte_pattern.decode(errors='ignore') if byte_pattern in ret else None

        return self._recv_data_until_match(match_func, timeout)

    def wait_for_multiple_bytes(self, byte_patterns, timeout=30):
        """
        Waits for any of multiple byte patterns to be matched.

        Args:
            byte_patterns (list of bytes): A list of byte patterns to wait for.
            timeout (int): Timeout for the channel. Default is 30 seconds.

        Returns:
            tuple: The received data and the matching byte pattern.
        """

        def match_func(ret):
            for pattern in byte_patterns:
                if pattern in ret:
                    return pattern
            return None

        return self._recv_data_until_match(match_func, timeout)

    def wait_for_regex(self, patterns, timeout=30):
        """
        Waits for any of multiple regular expression patterns to be matched.

        Args:
            patterns (list of str): A list of regular expression patterns to match.
            timeout (int): Timeout for the channel. Default is 30 seconds.

        Returns:
            tuple: The received data and the matching string fragment.
        """
        compiled_patterns = [re.compile(pattern) for pattern in patterns]

        def match_func(ret):
            decoded_ret = ret.decode(errors='ignore')
            for pattern in compiled_patterns:
                match = pattern.search(decoded_ret)
                if match:
                    return match.group()  # Return the matched string fragment
            return None

        return self._recv_data_until_match(match_func, timeout)

    def wait_for_multiple_str(self, patterns, timeout=30):
        r, match_bytes = self.wait_for_multiple_bytes([pattern.encode() for pattern in patterns], timeout)
        return r, match_bytes.decode()

    def raw_send_wait2(self, data, patterns, *args, **kwargs):
        self.raw_send(data)
        patterns = [patterns] if isinstance(patterns, str) else patterns
        patterns = [pattern.encode() for pattern in patterns]
        patterns.append(self.until_str.encode() if isinstance(self.until_str, str) else self.until_str)
        r, m = self.wait_for_multiple_bytes(patterns, *args, **kwargs)
        return r.decode(), m.decode()

    def recv_buffer_fix(self, sleep_time=0.2, recv_buff_size=9999):
        # 上条命令有未接收完的数据，需要继续接收一次，避免和后续的命令混淆
        time.sleep(sleep_time)
        if self.chan.recv_ready():
            r = self.chan.recv(recv_buff_size)
            sys.stdout.buffer.write(r)
            sys.stdout.flush()

    def raw_send_wait3(self, data, patterns=None, append_default_until_str=True, *args, **kwargs):
        self.raw_send(data)
        if patterns is None:
            patterns = []
        patterns = [patterns] if isinstance(patterns, str) else patterns
        patterns = [pattern.encode() for pattern in patterns]
        if append_default_until_str or not patterns:
            patterns.append(self.until_str.encode() if isinstance(self.until_str, str) else self.until_str)
        r, m = self.wait_for_multiple_bytes(patterns, *args, **kwargs)
        return r.decode(), m.decode()

    def raw_send_wait_simple(self, data, patterns=None, *args, **kwargs):
        r, m = self.raw_send_wait3(data, patterns, *args, **kwargs)
        return m
    def get_until_str(self):
        return self.until_str if isinstance(self.until_str, str) else self.until_str.decode()

def ssh_put_file(ssh, local_path, remote_path):
    remote_path = remote_path.replace(os.sep, posixpath.sep)
    print(f"{local_path} => {remote_path}", flush=True)
    ssh.scp_put(local_path, remote_path)


def ssh_put_file_rel(ssh, local_path, local_file_name, remote_path, remote_file_name):
    ssh_put_file(ssh, os.path.join(local_path, local_file_name), os.path.join(remote_path, remote_file_name))


def ssh_put_file_rel_v2(ssh, local_path, local_file_name, remote_path):
    ssh_put_file(ssh, os.path.join(local_path, local_file_name), os.path.join(remote_path, local_file_name))


def ssh_put_file_rel_v3(ssh, local_full_path, remote_path):
    file_name = os.path.basename(local_full_path)
    ssh_put_file(ssh, local_full_path, os.path.join(remote_path, file_name))


def ssh_put_file_rel_v4(ssh, local_path, wildcard_file_name, remote_path):
    local_full_path = glob.glob(f'{local_path}/{wildcard_file_name}')[0]
    ssh_put_file_rel_v3(ssh, local_full_path, remote_path)


def ssh_put_file_rel_one(ssh, local_path, wildcard_file_name, remote_path):
    # 使用通配符查找文件，并上传第一个文件
    pathname = f'{local_path}/{wildcard_file_name}'
    paths = glob.glob(pathname)
    if len(paths) == 0:
        assert False, f"未定位到{pathname}, 请检查"
    local_full_path = glob.glob(f'{local_path}/{wildcard_file_name}')[0]
    ssh_put_file_rel_v3(ssh, local_full_path, remote_path)


def ssh_put_file_rel_one_remote_full_path(ssh, local_path, wildcard_file_name, remote_path):
    local_full_path = glob.glob(f'{local_path}/{wildcard_file_name}')[0]
    ssh_put_file(ssh, local_full_path, remote_path)


def ssh_put_file_rel_all(ssh, local_path, wildcard_file_name, remote_path):
    # 使用通配符查找文件，并上传所有个文件
    for local_full_path in glob.glob(f'{local_path}/{wildcard_file_name}'):
        ssh_put_file_rel_v3(ssh, local_full_path, remote_path)


def reset_dir(ssh, path):
    ssh.exec(f"rm -rf {path}")
    assert not ssh.is_dir_exist(path)
    ssh.exec(f"mkdir -p {path}")
    assert ssh.is_dir_exist(path)


def delete_file(ssh, path):
    ssh.exec(f"rm -rf {path}")
    assert not ssh.is_file_exist(path)


def reset_dir_sudo(ssh, path):
    ssh.exec_sudo(f"rm -rf {path}")
    assert not ssh.is_dir_exist(path)
    ssh.exec_sudo(f"mkdir -p {path}")
    assert ssh.is_dir_exist(path)


def delete_file_sudo(ssh, path):
    ssh.exec_sudo(f"rm -rf {path}")
    assert not ssh.is_file_exist(path)


def update_to_server(ntls_client_ssh, bins_dir, installer_dir):
    ntls_client_ssh.reset_dir(installer_dir)
    if bins_dir.startswith("/"):
        ntls_client_ssh.exec_chan5(fr"\cp -v {bins_dir}/* {installer_dir}")
    else:
        for component in [
            "*",
        ]:
            ssh_put_file_rel_all(ntls_client_ssh,
                                 bins_dir,
                                 component,
                                 installer_dir)


def ping_until_ok(ssh, dst, try_times=2, bind_IP=None, ipv6=False):
    @call_until_success(try_times=try_times)
    def _ping():
        ipv6_str = "-6" if ipv6 else ""
        bind_ip_str = f"-I {bind_IP}" if bind_IP else ""
        cmd = f"ping {ipv6_str} {bind_ip_str} -c 1 -W 1 {dst}"

        print("_ping", cmd)
        code, output = ssh.exec2(cmd)
        if code != 0:
            raise RetryException()

    _ping()


def ping_fail_check(ssh, dst, **args):
    try:
        ping_until_ok(ssh, dst, **args)
        assert False, f"{dst} ping通了，请检查原因"
    except RetryOvertimesException:
        return


def ssh_chan(cfg, timeout=600, **kwargs) -> SshHelper:
    if "ctx" not in cfg and "ctx" not in kwargs:
        kwargs["ctx"] = cfg
    ssh = SshHelper(**cfg, **kwargs)

    ssh.start_chan(timeout=timeout, **merge_dicts_deep(cfg, kwargs))
    return ssh


def ssh_chan_factory(cfg, **kwargs):
    def f():
        return ssh_chan(cfg, **kwargs)

    return f


def gen_ssh_fixture(ssh_param):
    @pytest.fixture()
    def ssh():
        return ssh_chan(ssh_param, ctx=ssh_param)

    return ssh


def gen_value_fixture(param):
    @pytest.fixture(scope="session")
    def value():
        return param

    return value


def filter_servers(names, servers):
    return [server for server in servers if server.name in names]


def server_ssh_names(servers):
    return [server.name + "_ssh" for server in servers]


def server_ssh_names_by_server_name(server_names):
    return [server_name + "_ssh" for server_name in server_names]


def make_ssh_group_fixture_params(server_names, servers_cfg):
    # 创建一个空的列表来存储排序后的服务器配置
    sorted_servers_cfg = []

    # 按照 server_names 的顺序，从 servers_cfg 中查找并添加对应的配置
    for server_name in server_names:
        # 找到服务器名匹配的配置
        matching_server = next((server for server in servers_cfg if server['name'] == server_name), None)
        if matching_server:
            sorted_servers_cfg.append(matching_server)
        else:
            assert False, f"未匹配到的服务器配置{server_name}"

    # 返回排序后的结果
    return {
        "params": sorted_servers_cfg,
        "ids": server_ssh_names_by_server_name(server_names),
    }


def init_all_ssh_fixture(globals, servers):
    @pytest.fixture(params=servers,
                    ids=server_ssh_names(servers))
    def all_ssh(request):
        return ssh_chan(request.param, ctx=request.param)

    globals["all_ssh"] = all_ssh


def init_group_ssh_fixture(globals, group_name, server_names, servers):
    @pytest.fixture(**make_ssh_group_fixture_params(server_names, servers))
    def group_name_ssh(request):
        return ssh_chan(request.param, ctx=request.param)

    @pytest.fixture(**make_ssh_group_fixture_params(server_names, servers))
    def group_name_conf(request):
        return request.param

    globals[f"{group_name}_ssh"] = group_name_ssh
    globals[f"{group_name}_conf"] = group_name_conf


def init_servers_ssh_fixture(globals, servers):
    for server_conf in servers:
        globals[server_conf.name + "_ssh"] = gen_ssh_fixture(server_conf)
        globals[server_conf.name + "_conf"] = gen_value_fixture(server_conf)

    globals["servers_conf_fixture"] = gen_value_fixture(DotDict({
        server_conf.name: server_conf for server_conf in servers
    }))


def init_ssh_fixtures(globals, servers, groups=None):
    if not servers:
        return

    if isinstance(servers, dict):
        servers = [DotDict({"name": server_name, **servers[server_name]}) for server_name in servers.keys()]
    # 如果 groups 为 None，则初始化为空列表
    if groups is None:
        groups = []

    # 初始化所有服务器 all_ssh fixture
    init_all_ssh_fixture(globals, servers)

    # 初始化每个服务器 SSH fixture
    init_servers_ssh_fixture(globals, servers)

    all_server_names = [server.name for server in servers]
    # 遍历每个组，初始化服务器组 SSH fixture
    for group_name, group_server_names in groups:
        for server_name in group_server_names:
            assert server_name in all_server_names, f"未找到组{group_name}中{server_name}的配置，请检查"
        init_group_ssh_fixture(globals, group_name, group_server_names, servers)
