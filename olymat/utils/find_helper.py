def get_list_item_by_key(lst, key, val):
    '''
    在dict组成的list中，用dict中的key去查找
    找到返回 匹配dict
    未找到返回 None
    '''

    return next((sub for sub in lst if sub[key] == val), None)


def get_list_val_kvk(lst, key, val, key2):
    '''
    在dict组成的list中，用dict中的key去查找
    返回匹配的dict对象中key2的值
    '''

    d = next((sub for sub in lst if sub[key] == val), None)
    assert d, f"没能在list中定位到{key}={val}的对象"
    return d[key2]


def get_list_item_by_key_key(lst, key_root, key, val):
    '''
    在dict组成的list中，还有一层dict, 用dict中的key去第二层查找
    找到返回 匹配dict
    未找到返回 None
    如下： key_root为ApplyConfig，查找app_name
    [{
		"ApplyConfig": {
			"app_name": "default_lan1",
		}
	}, {
		"ApplyConfig": {
			"app_name": "default_lan2",

		}
	}, {
		"ApplyConfig": {
			"app_name": "default_lan3",
		}
	}]
    '''

    return next((sub[key_root] for sub in lst if sub[key_root][key] == val), None)


def test_get_list_item_by_key_key():
    d = [{
        "ApplyConfig": {
            "app_name": "default_lan1",
        }
    }, {
        "ApplyConfig": {
            "app_name": "default_lan2",

        }
    }, {
        "ApplyConfig": {
            "app_name": "default_lan3",
        }
    }]
    print(get_list_item_by_key_key(d, "ApplyConfig", "app_name", "default_lan3"))
