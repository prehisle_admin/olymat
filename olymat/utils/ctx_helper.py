import os
import time
from os import makedirs

from olymat.utils.dict_helper import DotDict
from olymat.utils.dir_helper import make_rel_path
from olymat.utils.json_helper import save_json, load_json
from olymat.utils.string_helper import basex_encode, ALPHABET_26


class TmpConf():
    def __init__(self, tmp_path, conf_name):
        self.tmp_path = tmp_path
        self.conf_name = conf_name

    def __enter__(self):
        makedirs(self.tmp_path, exist_ok=True)
        self.tconf_path = os.path.join(self.tmp_path, f"{self.conf_name}.json")
        if not os.path.exists(self.tconf_path):
            save_json(self.tconf_path, {})
        d = load_json(self.tconf_path)
        self.tconf = DotDict(d)
        return self.tconf

    def save(self):
        save_json(self.tconf_path, self.tconf)

    def __exit__(self, type, value, traceback):
        self.save()


class TmpConf2():
    def __init__(self, tmp_path, conf_name):
        self.tmp_path = tmp_path
        self.conf_name = conf_name
        self.init()

    def init(self):
        makedirs(self.tmp_path, exist_ok=True)
        self.tconf_path = os.path.join(self.tmp_path, f"{self.conf_name}.json")
        if not os.path.exists(self.tconf_path):
            save_json(self.tconf_path, {})
        d = load_json(self.tconf_path)
        self.tconf = DotDict(d)

    def get_conf_obj(self):
        return self.tconf

    def save(self):
        save_json(self.tconf_path, self.tconf)


def make_default_ctx(rel_path, conf_name="ctx"):
    with TmpConf(make_rel_path(rel_path, "tmp"), conf_name) as obj:
        yield obj

def read_ctx_data(rel_path, conf_name="ctx"):
    return TmpConf2(make_rel_path(rel_path, "tmp"), conf_name).get_conf_obj()

def reset_ctx(ctx):
    ctx.clear()
    ctx.iid = get_iid()
    ctx.tid = basex_encode(ctx.iid, ALPHABET_26, 2)


def get_iid():
    time.sleep(0.001)
    return int(time.time() * 1000 % 10000000)


def get_tid(fix_width=5):
    iid = get_iid()
    tid = basex_encode(iid, ALPHABET_26, fix_width)  # 通过iid生成字符
    return tid
