import functools
import logging
import time
import traceback
from functools import wraps

from olymat.utils.log_helper import logw


class RetryException(BaseException):
    pass


class RetryOutSideException(BaseException):
    pass


class RetryOvertimesException(BaseException):
    pass


class TimeoutException(BaseException):
    pass


def call_until_success(retry_exceptions=None,  # 重试的异常列表
                       first_sleep_time=0,  # 首次休眠几秒
                       sleep_time=1,  # 每次调用间停几秒
                       try_times=120,  # 最多重度几次
                       over_time=300,  # 秒,0不检测
                       over_times_raise_exception_class=RetryOvertimesException,  # 超过限制后产生的异常
                       detail_log=False,
                       ):
    if retry_exceptions is None:
        retry_exceptions = []

    def dec(func):
        all_retry_exceptions = (RetryException, *retry_exceptions)

        def __decorator(*args, **kwargs):
            """
            调用fun,直到成功
            """
            times = 0
            start_time = time.time()
            while 1:
                try:
                    if times == 0 and first_sleep_time:
                        logging.info("%s首次调用停%s秒" % (func.__name__, first_sleep_time))
                        time.sleep(first_sleep_time)

                    result = func(*args, **kwargs)
                    func._olymat_retry_times = times
                    return result

                except all_retry_exceptions as e:
                    # exc_info = sys.exc_info()
                    times = times + 1
                    elapsed_time = time.time() - start_time
                    # traceback.print_exception(*exc_info)
                    if detail_log:

                        logging.info(
                            f"call_until_success retry %s:%s elapsed_time:{elapsed_time:.2f}s [%s]\n%s\n%s" % (
                                times, e.__class__.__name__ + "." + str(e), func.__name__, args,
                                "".join(
                                    # traceback.format_stack(limit=3)[1::-1])
                                    filter(
                                        lambda s: all(
                                            [x not in s for x in [
                                                "<frozen",
                                                "PyCharm",
                                                "_pytest",
                                                "pluggy",
                                                "pytest.exe",
                                                # "site-packages",
                                                "control_helper.py"
                                            ]]),
                                        traceback.format_stack())
                                )))
                    else:
                        logging.info(f"%s重试%s次,elapsed_time:{elapsed_time:.2f}s, %s:%s" % (func.__name__, times, e.__class__.__name__ + "." + str(e), args))

                    if over_time > 0 and elapsed_time > over_time:
                        raise over_times_raise_exception_class(
                            f"[%s]超过限时%s elapsed_time:{elapsed_time:.2f}s,放弃" % (func.__name__, over_time))
                    if try_times > 0 and times >= try_times:
                        raise over_times_raise_exception_class(
                            f"[%s]重试次数超过%s elapsed_time:{elapsed_time:.2f}s,放弃" % (func.__name__, try_times))
                    time.sleep(sleep_time)

        return __decorator

    return dec


def exception_with_default(default_v, exceptions=None, val2default=None):
    '''
    该函数是个装饰器，在发生异常时，返回默认值
    @param exceptions: 需要捕获的异常
    @type default_v: 任意值

    '''
    if exceptions is None:
        exceptions = []

    def dec(func):
        all_retry_exceptions = (*exceptions,)

        def __decorator(*args, **kwargs):
            try:
                val = func(*args, **kwargs)
                if val == val2default:
                    return default_v
                return val
            except all_retry_exceptions as e:
                logw(f"exception_with_default {e}")
                return default_v

        return __decorator

    return dec


call_until_success_timeout = functools.partial(call_until_success, over_time=300)


def result_verify(verify_func):
    def decorator(func):
        def wrapper(*args, **kwargs):
            skip_verify = kwargs.pop('skip_verify', False)
            result = func(*args, **kwargs)
            if not skip_verify:
                verify_func(result)
            return result

        return wrapper

    return decorator


def verify_func(response):
    assert response["statusCode"] == 200, response


def result_verify2(verify_func):
    def decorator(func):
        def wrapper(*args, **kwargs):
            skip_verify = kwargs.pop('skip_verify', False)
            result = func(*args, **kwargs)
            if not skip_verify:
                result = verify_func(result)
            return result

        return wrapper

    return decorator


def verify_func2(response):
    j = response.json()
    assert j["statusCode"] == 200, j
    return j


def before_after_hook_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        before_hook_f = None
        if "before_hook" in kwargs:
            before_hook_f = kwargs["before_hook"]
            kwargs.pop("before_hook")

        if before_hook_f:
            before_hook_f(args, kwargs)

        after_hook_f = None
        if "after_hook" in kwargs:
            after_hook_f = kwargs["after_hook"]
            kwargs.pop("after_hook")

        r = func(*args, **kwargs)

        if after_hook_f:
            return after_hook_f(r, *args, **kwargs)
        else:
            return r

    return wrapper


def add_kwargs_decorator(**additional_kwargs):
    """
    装饰器工厂，接受任意的关键字参数，并在装饰的函数中添加这些参数到 kwargs。

    :param additional_kwargs: 要添加到函数 kwargs 的额外参数
    """

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # 将额外的关键字参数添加到 kwargs 中
            kwargs.update(additional_kwargs)
            # 调用原函数，并传递修改后的 kwargs
            return func(*args, **kwargs)

        return wrapper

    return decorator


def conf_check(cfg, need_conf_type):
    assert cfg.conf_type == need_conf_type, f"当前配置类型为'{cfg.conf_type}'，请切换为'{need_conf_type}'的配置文件"