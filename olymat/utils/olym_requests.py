import logging
import shutil
from functools import wraps

import urllib3

from olymat.utils.control_helper import before_after_hook_decorator
from olymat.utils.dict_helper import merge_dicts_deep

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import requests

# olym_requests.TIMEOUT = (1, 1) # 要这样使用
TIMEOUT = (30, 30)  # 连接超时，读取超时


def new_session(http_proxy=None, user_agent=None, baseurl=None, **g_kwargs):
    class MySessions(requests.Session):
        def __init__(self, baseurl=baseurl, *args, **kwargs):
            super(MySessions, self).__init__(*args, **kwargs)
            self.baseurl = baseurl

        @modify_params_and_json_decorator
        def request(self, method, url, *args, **kwargs):
            # 如果 baseurl 存在，则将其与传入的相对路径拼接
            if self.baseurl and url.startswith("/"):
                url = self.baseurl + url
            for key, val in g_kwargs.items(): # 优先使用kwargs
                kwargs.setdefault(key, val) # kwargs中没有g_kwargs的配置，则使用g_kwargs

            kwargs.setdefault('timeout', TIMEOUT)  # 没有timeout时，使用全局超时

            for name in ["name", "update_data", "delete_items", "update_params", "update_json", "update_data",
                         "delete_params", "delete_json", "delete_data"]:
                if name in kwargs:
                    kwargs.pop(name)  # 适配locust
            return super(MySessions, self).request(method, url, *args, **kwargs)

    s = MySessions()  # 使用自定议超时
    if http_proxy:
        s.proxies = {
            'http': http_proxy,
            'https': http_proxy,
        }
    user_agent = user_agent if user_agent else "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.54 Safari/537.36"
    s.headers = {
        'User-Agent': user_agent,
    }
    s.verify = False
    return s


def new_session_v2(http_proxy=None, user_agent=None, baseurl=None, **g_kwargs):
    class MySessions(requests.Session):
        def __init__(self, baseurl=baseurl, *args, **kwargs):
            super(MySessions, self).__init__(*args, **kwargs)
            self.baseurl = baseurl

        @before_after_hook_decorator
        def request(self, method, url, *args, **kwargs):
            # 如果 baseurl 存在，则将其与传入的相对路径拼接
            if self.baseurl and url.startswith("/"):
                url = self.baseurl + url
            for key, val in g_kwargs.items():  # 优先使用kwargs
                kwargs.setdefault(key, val)  # kwargs中没有g_kwargs的配置，则使用g_kwargs

            kwargs.setdefault('timeout', TIMEOUT)  # 没有timeout时，使用全局超时

            return super(MySessions, self).request(method, url, *args, **kwargs)

    s = MySessions()  # 使用自定议超时
    if http_proxy:
        s.proxies = {
            'http': http_proxy,
            'https': http_proxy,
        }
    user_agent = user_agent if user_agent else "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.54 Safari/537.36"
    s.headers = {
        'User-Agent': user_agent,
    }
    s.verify = False
    return s


def update_http_proxy(s, http_proxy=None):
    s.proxies = {
        'http': http_proxy,
        'https': http_proxy,
    }


def downloadfile(url, local_file_path):
    logging.debug("downloadfile %s->%s" % (url, local_file_path))
    r = requests.get(url, stream=True)
    assert r.status_code == 200, r.text
    with open(local_file_path, 'wb') as f:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, f)


def downloadfile_session(s, url, local_file_path):
    logging.debug("downloadfile_session %s->%s" % (url, local_file_path))
    r = s.get(url, stream=True)
    assert r.status_code == 200, r.text
    with open(local_file_path, 'wb') as f:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, f)


def verify_decorator(verification_func):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            return func(self, *args, **kwargs)

        def verify(self, *args, **kwargs):
            result = func(self, *args, **kwargs)
            return verification_func(result)

        wrapper.verify = verify
        return wrapper

    return decorator


def verify_status_code_200(result):
    assert (result["statusCode"] == 200), result
    return result


class VerifyMeta(type):
    def __new__(cls, name, bases, dct):
        new_cls = super().__new__(cls, name, bases, dct)
        for attr_name, attr_value in dct.items():
            if callable(attr_value) and hasattr(attr_value, 'verify'):
                setattr(new_cls, attr_name, attr_value)
                setattr(new_cls, f'{attr_name}_verify', attr_value.verify)
        return new_cls


# VerifyMeta, verify_decorator使用示例
# class CPMonitoring(metaclass=VerifyMeta):
#     def __init__(self, s):
#         self.s = s
#
#     @verify_decorator(verify_status_code_200)
#     def cipher_monitor_monitoring_company_list(self):
#         r = self.s.get("https://192.168.17.26:8443/cipher_monitor/monitoring/company/list")
#         return r.json()
#
# # 使用示例
# def test_s(s):
#     cpm = CPMonitoring(s)
#     result = cpm.cipher_monitor_monitoring_company_list()
#     print(result)
#     verify_result = cpm.cipher_monitor_monitoring_company_list_verify()
#     print(verify_result)
def modify_params_and_json_decorator(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        for modify_name in ["json", "params", "data"]:
            if modify_name in kwargs:
                if f"update_{modify_name}" in kwargs and kwargs[f"update_{modify_name}"]:
                    kwargs[modify_name] = merge_dicts_deep(kwargs[modify_name], kwargs[f"update_{modify_name}"])
                    del kwargs[f"update_{modify_name}"]

                if f"delete_{modify_name}" in kwargs and kwargs[f"delete_{modify_name}"]:
                    for item in kwargs[f"delete_{modify_name}"]:
                        try:
                            del kwargs[modify_name][item]
                        except KeyError:
                            pass
                    del kwargs[f"delete_{modify_name}"]

        # 调用原始函数，并获取credentials
        return func(*args, **kwargs)

    return wrapper
