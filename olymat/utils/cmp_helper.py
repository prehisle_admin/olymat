import operator

from packaging import version

OPERATOR_DICT = {
    '==': operator.eq,
    '!=': operator.ne,
    '<': operator.lt,
    '<=': operator.le,
    '>': operator.gt,
    '>=': operator.ge,
}


def ver_cmp(current_version, op, compared_version):
    return OPERATOR_DICT[op](version.parse(current_version), version.parse(compared_version))


class VersionComparable:
    def __init__(self, version_str):
        self.version = version.parse(version_str)

    def __eq__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version == other

    def __ne__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version != other

    def __lt__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version < other

    def __le__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version <= other

    def __gt__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version > other

    def __ge__(self, other):
        if isinstance(other, str):
            other = version.parse(other)
        return self.version >= other


def test_ver_cmp():
    assert ver_cmp('1.0.0', '==', '1.0.0')
    assert ver_cmp('1.0.0', '!=', '1.0.1')
    assert ver_cmp('1.0.0', '<', '1.0.1')
    assert ver_cmp('1.0.0', '<=', '1.0.1')
    assert ver_cmp('1.0.1', '>', '1.0.0')
    assert ver_cmp('1.0.1', '>=', '1.0.0')
    assert ver_cmp('1.0.1', '>=', '1.0.1')
    assert ver_cmp('1.0.1', '!=', '1.0.0')
    assert ver_cmp('1.0.1', '==', '1.0.1')


def test_VersionComparable():
    assert VersionComparable("1.0.0") == "1.0.0"
    assert VersionComparable("1.0.1") != "1.0.0"
    assert VersionComparable("1.0.1") >= "1.0.0"
    assert VersionComparable("1.0.1") >= "1.0.1"
    assert VersionComparable("1.0.1") > "1.0.0"
    assert VersionComparable("1.0.0") < "1.0.1"
    assert VersionComparable("1.0.0") <= "1.0.1"
    assert "1.0.0" <= VersionComparable("1.0.1")
    assert "1.0.0" == VersionComparable("1.0.0")
