import os
import re
from urllib.parse import urlparse, urljoin

import requests
from bs4 import BeautifulSoup

from olymat.utils.log_helper import logi


def extract_last_segment_from_url(url):
    try:
        # 使用 urlparse 解析 URL
        parsed_url = urlparse(url)

        # 获取 URL 的路径部分，并使用 '/' 分割
        path_segments = parsed_url.path.split('/')

        # 从路径中提取最后一段
        last_segment = path_segments[-1]

        return last_segment

    except Exception as e:
        print(f"Error: {e}")
        return None


def download_large_file(url, local_filename):
    try:
        logi(f"{url} File downloaded and saved as {local_filename} begin")
        with requests.get(url, stream=True) as response:
            response.raise_for_status()  # 检查是否有错误发生

            with open(local_filename, 'wb') as file:
                for chunk in response.iter_content(chunk_size=8192):
                    if chunk:
                        file.write(chunk)

        logi(f"{url} File downloaded and saved as {local_filename} end")
    except requests.exceptions.RequestException as e:
        logi(f"Download error: {e}")
        assert False

def download_large_file_v2(s, url, local_filename, params=None):
    try:
        logi(f"{url} File downloaded and saved as {local_filename} begin")
        with s.get(url, params=params, stream=True) as response:
            response.raise_for_status()  # 检查是否有错误发生

            with open(local_filename, 'wb') as file:
                for chunk in response.iter_content(chunk_size=8192):
                    if chunk:
                        file.write(chunk)

        logi(f"{url} File downloaded and saved as {local_filename} end")
    except requests.exceptions.RequestException as e:
        logi(f"Download error: {e}")
        assert False

def filter_urls_by_regex(urls, regex_pattern):
    filtered_urls = []

    for url in urls:
        if re.match(regex_pattern, url):
            filtered_urls.append(url)

    return filtered_urls


def extract_all_urls(url):
    if not url.endswith("/"):
        url += "/"
    try:
        # 发送 HTTP GET 请求获取页面内容
        response = requests.get(url)
        response.raise_for_status()  # 检查是否有错误发生

        # 使用 BeautifulSoup 解析页面内容
        soup = BeautifulSoup(response.text, 'html.parser')

        # 查找所有的<a>标签，提取其中的href属性作为 URL
        urls = []
        for link in soup.find_all('a'):
            href = link.get('href')
            if href:
                # 使用urljoin函数将相对路径转换为绝对路径
                absolute_url = urljoin(url, href)
                urls.append(absolute_url)

        return urls

    except requests.exceptions.RequestException as e:
        print(f"Error: {e}")
        return []


def download_all_by_urls(url, objdir, regex_pattern=".*"):
    logi(f"download_all_by_urls {url} {objdir} {regex_pattern}")
    urls = extract_all_urls(url)
    urls = filter_urls_by_regex(urls, regex_pattern)
    for url in urls:
        filename = extract_last_segment_from_url(url)
        download_large_file(url, os.path.join(objdir, filename))

def download_all_url_file(url, objdir):
    download_all_by_urls(url, objdir, r'^(?!.*\/$).*$')