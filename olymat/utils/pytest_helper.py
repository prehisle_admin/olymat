from olymat.utils.dict_helper import DotDict


def smart_parametrize(args_ids):
    """
    根据给定的参数ID列表进行参数化处理。

    该函数接收一个包含参数ID和参数值的元组列表，然后将这些参数和ID分离成两个列表，
    并以字典形式返回，便于测试函数使用不同的参数进行测试。

    参数:
    args_ids -- 一个元组列表，每个元组包含两个元素，第一个元素是参数ID，第二个元素是参数值。

    返回值:
    一个字典，包含两个键值对：
    - "argvalues": 参数值的列表。
    - "ids": 参数ID的列表。
    """
    # 提取参数值，用于测试函数的参数化
    params = [DotDict(v[1]) if isinstance(v[1], dict) else v[1] for v in args_ids]
    # 提取参数ID，用于标识每组参数化数据
    ids = [v[0] for v in args_ids]
    # 返回参数化数据和ID的字典
    return {
        "argvalues": params,
        "ids": ids
    }


# @pytest.mark.parametrize("xx, bb", **pretty_parametrize([
#     ("x1", (1, 2)),
#     ("x2", (1, 2)),
# ]))
# def test_xx(xx, bb):
#     print(xx, bb)

import pytest

from olymat.mgr import mgr
from olymat.utils.dir_helper import delete_old_files
from olymat.utils.log_helper import logp, logw


def pytest_testif(condition, reason="", **kwargs):
    return pytest.mark.skipif(not condition, reason=reason, **kwargs)


def pytest_skipif(condition, reason="", **kwargs):
    return pytest.mark.skipif(condition, reason=reason, **kwargs)


def pytest_module_testif(reason, condition, **kwargs):
    if not condition:
        pytest.skip(reason, allow_module_level=True, **kwargs)


def pytest_module_skipif(reason, condition, **kwargs):
    if condition:
        pytest.skip(reason, allow_module_level=True, **kwargs)


def update_olymat_conf(config, olymat_conf):
    config.safeMode = False
    try:
        if getattr(config, "olymat_conf", None):
            print("olymat_conf already init..")
            return False
        config.olymat_conf = olymat_conf
        if not config.olymat_conf:
            config.safeMode = True
            logp("测试集中未加载到配置, 进入保护模式, 不通知、不录屏、不生成报告")
            return False  # 测试集中未加载配置, 直接在根conftest.py加载的配置，这里要忽略
        return True
    except FileNotFoundError as e:
        logp(f"加载配置失败{str(e)}, 进入保护模式")  # pytest_configure中不要用logging输出，否则会出现两份日志输出在屏幕
        config.safeMode = True
        return False


def init_olymat_conf(config):
    # args = shlex.split("-x")
    # config.args.extend(args)
    # config.option.maxfail = 1

    # https://docs.pytest.org/en/stable/how-to/cache.html
    if config.olymat_conf.pytest_conf:
        for key, value in config.olymat_conf.pytest_conf.items():
            if key in config.option:
                setattr(config.option, key, value)
            else:
                logw(f"pytest_conf中配置的参数{key}在pytest中未定义")

        if "stepwise_skip" in config.olymat_conf.pytest_conf:  # 当pytest.ini配置有-x时, stepwise_skip优先级低, 第一个错误就会停止
            config.option.maxfail = 0

    if "main.py" in config.args[0]:
        config.olymat_conf.enable_html_report = False
        config.olymat_conf.send_notify.enable = False

    if config.olymat_conf.enable_html_report:
        tid = mgr.inc_testid()
        delete_old_files(f"reports/{config.olymat_conf._ctx.tests_dir_name}", 20)
        html_report_file = f"reports/{config.olymat_conf._ctx.tests_dir_name}/{tid}.report.html"
        config._metadata = None
        config.pluginmanager.import_plugin("pytest_html")
        config.option.htmlpath = html_report_file
        config.option.self_contained_html = True
