def split_n(lst: list, n):
    """
    将列表分割成包含n个元素的子列表。

    参数:
    lst: list - 需要被分割的原始列表。
    n: int - 每个子列表应包含的元素数量。

    返回值:
    list - 由长度为n的子列表组成的新的列表。
    """
    return [lst[i:i + n] for i in range(0, len(lst), n)]
