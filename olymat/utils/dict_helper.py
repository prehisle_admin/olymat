class DotDict(dict):
    __delattr__ = dict.__delitem__

    def __init__(self, dct=None):
        if dct:
            for k, v in dct.items():
                self.__setitem__(k, v)

    def __getitem__(self, k):
        v = self.get(k, "__DotNone")
        if v == "__DotNone" and k != "shape" and k != "_asdict":
            super().__setitem__(k, DotDict({}))
            return self.get(k)
        else:
            return v

    def __getattr__(self, k):
        return self.__getitem__(k)

    def __setattr__(self, k, v):
        self.__setitem__(k, v)

    def __setitem__(self, k, v):
        if isinstance(v, dict):
            v = DotDict(v)
        elif isinstance(v, list):
            v = [DotDict(item) if isinstance(item, dict) else item for item in v]
        super().__setitem__(k, v)

    def get_with_default(self, key_str, default_val):
        v = self
        for k in key_str.split("."):
            v = v.get(k)
            if v is None:
                return default_val
        return v


def extract_keys(original_dict, keys):
    result_dict = {key: original_dict[key] for key in keys if key in original_dict}
    return result_dict

def merge_dicts(data1, data2):
    # 将data2中在data1中存在的键值对合并到data1中，对于data2中的其他键值对应该被忽略
    for key, value in data2.items():
        if key in data1:
            data1[key] = value


def merge_dicts_deep(obj1, obj2, list_patch=True):
    if isinstance(obj1, dict) and isinstance(obj2, dict):
        return _merge_dicts_deep(obj1, obj2, list_patch)
    elif isinstance(obj1, list) and isinstance(obj2, list):
        return merge_lists_by_index2(obj1, obj2, list_patch)
    else:
        # 如果类型不匹配，优先返回 obj2 的值
        return obj2
def _merge_dicts_deep(dict1, dict2, list_patch=True):
    result = dict1.copy()
    for key, value in dict2.items():
        if isinstance(value, dict) and key in result and isinstance(result[key], dict):
            result[key] = merge_dicts_deep(result[key], value)
        else:
            result[key] = value
    return result


def merge_dicts_deep2(dict1, dict2, list_patch=False):
    result = dict1.copy()

    for key, value in dict2.items():
        if key in result:
            if isinstance(result[key], dict) and isinstance(value, dict):
                # 递归合并嵌套的字典
                result[key] = merge_dicts_deep2(result[key], value, list_patch)
            elif isinstance(result[key], list) and isinstance(value, list):
                # 深度合并列表中的字典
                result[key] = merge_lists_by_index2(result[key], value, list_patch)
            else:
                # 如果是其他类型，直接替换
                result[key] = value
        else:
            result[key] = value

    return result


def merge_lists_by_index2(list1, list2, list_patch=False):
    """
    根据索引深度合并两个列表中的字典项。对于 list2 中超过 list1 长度的部分，直接附加。
    """
    merged_list = list1.copy()

    # 根据索引进行深度合并
    for i, item2 in enumerate(list2):
        if i < len(merged_list):
            item1 = merged_list[i]
            if list_patch and isinstance(item1, dict) and isinstance(item2, dict):
                # 如果同一位置的元素是字典，递归合并
                merged_list[i] = merge_dicts_deep2(item1, item2, list_patch)
            else:
                # 如果类型不同或非字典，直接替换
                merged_list[i] = item2
        else:
            # 超出 list1 长度的部分直接附加
            merged_list.append(item2)
    if not list_patch and len(merged_list) > len(list2): # 待合并的长度更小则移除原list的尾部
        merged_list = merged_list[:len(list2)]

    return merged_list