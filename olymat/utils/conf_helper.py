def parse_list_conf(item_conf):
    '''
    用于支持这种配置'@python aaaa{:03d},1,99' # 用于一组名称相似的用户，这里会生成aaaa001到aaaa099共99个用户
    :param item_conf: 配置字符串或配置数组，支持区间替换
    :return:
    '''
    if isinstance(item_conf, str):
        if item_conf.startswith("@python"):
            fstr, start, end = map(str.strip, item_conf.lstrip("@python").split(","))
            accounts = [fstr.format(i) for i in range(int(start), int(end) + 1)]
        elif item_conf.startswith("@jinja2"):
            accounts = exec(item_conf.lstrip("@jinja2").strip())
        else:
            accounts = [item_conf]
    else:
        accounts = item_conf

    return accounts

def parse_cmd_conf(item_conf):
    '''
    用于支持这种配置'@python aaaa{:03d},1,99' # 用于一组名称相似的用户，这里会生成aaaa001到aaaa099共99个用户
    :param item_conf: 配置字符串或配置数组，支持区间替换
    :return:
    '''
    result = item_conf
    if isinstance(item_conf, str):
        if item_conf.startswith("@python_list"):
            fstr, start, end = map(str.strip, item_conf.lstrip("@python_list").split(","))
            result = [fstr.format(i) for i in range(int(start), int(end) + 1)]
        elif item_conf.startswith("@jinja2"):
            result = eval(item_conf.lstrip("@jinja2").strip())

    return result
