import hashlib
import random
import string
import time
import uuid

from olymat.utils.string_helper import basex_encode, ALPHABET_26


def rand_bytes(min_len=10, max_len=10):
    return bytes(bytearray(random.getrandbits(8) for _ in range(random.randint(min_len, max_len))))


def rand_nbytes(bytes_len=10):
    return bytes(bytearray(random.getrandbits(8) for _ in range(bytes_len)))


def rand_data_hex(min_len=10, max_len=10):
    return rand_bytes(min_len, max_len).hex()


def rand_number_str(min_len=10, max_len=10):
    return ''.join([random.choice(string.digits) for _ in range(random.randint(min_len, max_len))])


def rand_printable_str(min_len=10, max_len=10):
    return ''.join([random.choice(string.printable) for _ in range(random.randint(min_len, max_len))])

def rand_n_str(n=10):
    return rand_str(string.ascii_letters, min_len=n, max_len=n)

def rand_nprintable_str(n=10):
    return rand_printable_str(min_len=n, max_len=n)

def rand_str(seed, min_len=10, max_len=10):
    return ''.join([random.choice(seed) for _ in range(random.randint(min_len, max_len))])

def rand_nstr(seed, n=10):
    return ''.join([random.choice(seed) for _ in range(n)])


def rand_ip():
    return '.'.join([str(random.randint(1, 254)) for x in range(4)])


def rand_port():
    return str(random.randint(1, 65535))


def nanoid(genlen=6, prefix=""):
    return prefix + hashlib.md5(uuid.uuid4().bytes).hexdigest()[:genlen]


def get_time_sn(gen_len=11):
    time.sleep(0.001)
    return int(time.time() * 1000 % (10 ** gen_len))

def rand_n_number(n=10):
    return rand_number_str(n, n)

def rand_phone_number():
    return "1" + rand_n_number(5) + str(get_time_sn(5))

def get_sn_str(seed_len=8):
    sn = get_time_sn(seed_len)
    sn_str = basex_encode(sn, ALPHABET_26, seed_len)
    return sn_str

def test_x():
    for _ in range(10):
        print(get_sn_str())

    print(time.time())


class RandomWeights():
    """
    一个用于生成基于权重的随机数据的类。

    参数:
    - data_weights: 一个包含数据和对应权重的列表，每个元素是一个二元组。(e.g., [(data1, weight1), (data2, weight2), ...])

    属性:
    - data_weights: 初始化时输入的数据权重列表。
    - prob_dict: 一个字典，键为数据，值为对应的数据权重，用于快速计算。
    """
    def __init__(self, data_weights):
        """
        初始化RandomWeights类的实例。

        参数:
        - data_weights: 同类说明中的data_weights。
        """
        self.data_weights = data_weights
        self.prob_dict = {}
        # 根据输入的数据权重列表构建概率字典
        for pair in self.data_weights:
            if pair[0] not in self.prob_dict:
                self.prob_dict[pair[0]] = pair[1]
            else:
                self.prob_dict[pair[0]] += pair[1]

    def gen(self):
        """
        根据存储的概率字典生成一个基于权重的随机数据。

        返回值:
        - 生成的随机数据，该数据是从基于权重的分布中选出的。
        """
        return random.choices(list(self.prob_dict.keys()), weights=self.prob_dict.values(), k=1)[0]


def test_RandomWeights():
    # 初始化数据，并创建RandomWeights实例
    data = [[1, 3], [2, 3], [3, 1], [4, 1]]
    rw = RandomWeights(data)

    # 循环10次，打印出每次调用gen()方法的结果
    for _ in range(10):
        print(rw.gen())

class RandomWeightsEx():
    """
    一个根据给定数据权重生成随机结果的类。

    Attributes:
        data_weights (list): 一个包含数据及其权重的列表，每个元素也是一个列表，第一个元素为数据标识，后续为权重，第三个是数据。
        prob_dict (dict): 根据data_weights构建的概率字典，键为数据标识，值为权重。
    """
    def __init__(self, data_weights):
        """
        初始化RandomWeightsEx实例。

        Parameters:
            data_weights (list): 包含数据及其权重的列表。
        """
        self.data_weights = data_weights
        self.prob_dict = {}
        # 根据输入的数据权重列表构建概率字典
        for pair in self.data_weights:
            if pair[0] not in self.prob_dict:
                self.prob_dict[pair[0]] = pair[1:]
            else:
                self.prob_dict[pair[0]] += pair[1:]

    def gen(self):
        """
        根据概率字典生成一个随机结果。

        Returns:
            list: 一个包含随机选择的数据及其权重的列表。
        """
        return self.prob_dict[random.choices(list(self.prob_dict.keys()), weights=[item[0] for item in self.prob_dict.values()], k=1)[0]][1:][0]

def test_RandomWeightsEx():
    """
    RandomWeightsEx类的测试函数。
    """
    data = [["name1", 3, "x1"], ["name2", 3, "x2"], ["name3", 2, "x3"], ["name4", 1, "x4"]]
    rw = RandomWeightsEx(data)

    # 循环调用gen方法并打印结果，以进行测试
    for _ in range(20):
        print(rw.gen())
    '''
    x4
    x2
    x2
    x2
    x3
    '''