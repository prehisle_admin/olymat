import logging

from olymat.utils.date_helper import get_cur_time


def update_logger(mode="sys", **kwargs):
    global logger
    if mode == "sys":
        logger = logging
    elif mode == "color":
        logger = get_color_logger(**kwargs)
    else:
        assert False, f"{mode} is not defined"


def get_color_logger(level=logging.INFO):
    import logging

    # ANSI escape sequences for colors
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(30, 38)

    RESET_SEQ = "\033[0m"
    COLOR_SEQ = "\033[1;%dm"

    COLORS = {
        'WARNING': YELLOW,
        'INFO': GREEN,
        'DEBUG': BLUE,
        'CRITICAL': MAGENTA,
        'ERROR': RED
    }

    class ColoredFormatter(logging.Formatter):
        def format(self, record):
            levelname = record.levelname
            if levelname in COLORS:
                formatted_message = logging.Formatter.format(self, record)
                return COLOR_SEQ % COLORS[levelname] + formatted_message + RESET_SEQ
            else:
                return logging.Formatter.format(self, record)

    # Create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(level)

    # Create formatter and add it to the handlers
    formatter = ColoredFormatter("%(asctime)s [%(levelname)s] %(message)s")
    ch.setFormatter(formatter)

    # Create logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    for handler in logger.handlers[:]:
        logger.removeHandler(handler)

    logger.addHandler(ch)
    return logger


logger: type[logging.Logger] = None
update_logger(mode="sys")


def logp(msg, *args, **kwargs):
    print(f"{get_cur_time()} {msg}, {args, kwargs}")


def logd(msg, *args, **kwargs):
    logger.debug(msg, *args, **kwargs)


def logi(msg, *args, **kwargs):
    logger.info(msg, *args, **kwargs)


def logw(msg, *args, **kwargs):
    logger.warning(msg, *args, **kwargs)


def loge(msg, exc_info=True, *args, **kwargs):
    logger.error(msg, exc_info=exc_info, *args, **kwargs)  # exc_info=True,


def logc(msg, *args, **kwargs):
    logger.critical(msg, *args, **kwargs)


def update_log_level(level=logging.INFO):
    logging.basicConfig(level=level)
