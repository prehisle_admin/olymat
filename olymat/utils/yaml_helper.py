from collections import OrderedDict

import os
import textwrap
import yaml
from jinja2 import Template
from typing import Mapping
from yaml import Dumper

from olymat.utils.dict_helper import DotDict, merge_dicts_deep2


def load_yaml(yaml_path):
    with open(yaml_path, encoding="utf-8") as f:
        return yaml.load(f, Loader=yaml.SafeLoader)


def load_yaml_str(yam_str):
    return yaml.load(yam_str, Loader=yaml.SafeLoader)


def load_yaml_str2(yam_str):
    return yaml.load(textwrap.dedent(yam_str), Loader=yaml.SafeLoader)


def update_yaml(yaml_file_path, new_data, list_patch=False):
    from ruamel.yaml import YAML
    yaml = YAML()
    yaml.preserve_quotes = True  # 保留原始引号
    yaml.indent(mapping=2, sequence=4, offset=2)  # 设置缩进格式
    # 读取 YAML 文件
    with open(yaml_file_path, 'r', encoding="utf-8") as file:
        data = yaml.load(file, )

    data = merge_dicts_deep2(data, new_data, list_patch)

    # 将修改后的数据写回文件
    with open(yaml_file_path, 'w', encoding="utf-8", newline='\n') as file:
        yaml.dump(data, file)


class VerboseSafeDumper(yaml.SafeDumper):
    def ignore_aliases(self, data):
        return True


class CompactDumper(yaml.SafeDumper):
    def represent_list(self, data):
        if all(isinstance(i, Mapping) for i in data):
            return self.represent_sequence('tag:yaml.org,2002:seq', data, flow_style=False)
        else:
            return self.represent_sequence('tag:yaml.org,2002:seq', data, flow_style=True)


VerboseSafeDumper.add_representer(list, CompactDumper.represent_list)


def represent_undefined(self, data):
    return self.represent_dict(data)


yaml.add_representer(type(DotDict({})), represent_undefined, Dumper=VerboseSafeDumper)


def save_yaml(yaml_file_path, data, use_aliases=False, **kwargs):
    dumper = Dumper if use_aliases else VerboseSafeDumper
    with open(yaml_file_path, 'w', encoding="utf-8") as file:
        # 使用dump方法将字典对象写入到文件中
        yaml.dump(data, file, sort_keys=False, allow_unicode=True, encoding="utf-8", Dumper=dumper, **kwargs)


def merge_dict(d1, d2, list_patch=False):
    return merge_dicts_deep2(d1, d2, list_patch)


def recurse_import_yaml(base_path, data, list_patch=False):
    if isinstance(data, dict):
        # 如果 `_import` 存在，提前将其移除并保存
        import_value = data.pop('_import', None)

        # 先处理所有非 `_import` 键
        for k, v in data.items():
            data[k] = recurse_import_yaml(base_path, v)

        # 最后处理 `_import` 键
        if import_value:
            if isinstance(import_value, list):
                for i in import_value:
                    data = import_and_merge(base_path, i, data, list_patch)
            elif isinstance(import_value, str):
                data = import_and_merge(base_path, import_value, data, list_patch)
    elif isinstance(data, list):
        for i in range(len(data)):
            data[i] = recurse_import_yaml(base_path, data[i], list_patch)

    return data


def import_and_merge(base_path, i, data, list_patch=False):
    if os.path.isabs(i):
        file_abspath = i
    else:
        file_abspath = os.path.join(base_path, i)
    import_data = import_parse_yaml(file_abspath)
    new_data = merge_dict(import_data, data, list_patch)
    return new_data


def import_parse_yaml(file_path):
    base_path = os.path.dirname(os.path.realpath(file_path))
    data = load_yaml(file_path)

    # create a new dict similar to data
    new_data = recurse_import_yaml(base_path, data)

    return new_data


def compose_yaml_str(src_yml_str, new_data, list_patch=False):
    tpl_data = load_yaml_str(textwrap.dedent(src_yml_str))
    if isinstance(new_data, str):
        new_data = load_yaml_str(textwrap.dedent(new_data))
    return merge_dict(tpl_data, new_data, list_patch)


def compose_yaml_file(src_yml, new_data, list_patch=False):
    tpl_data = import_parse_yaml(src_yml)
    if isinstance(new_data, str):
        new_data = load_yaml_str(textwrap.dedent(new_data))
    return merge_dict(tpl_data, new_data, list_patch)


def render_yaml(conf):
    """
    函数功能：使用Jinja2模板处理YAML配置文件。

    参数:
    conf (dict): 初始化的配置字典。

    返回:
    fconf (dict):  处理之后的配置字典。
    """

    yaml_str = yaml.dump(conf, default_flow_style=False, sort_keys=False)

    # 使用 jinja2 的 Template 方法，将上一步得到的待渲染的模板字符串转换为模板
    template = Template(yaml_str)
    # 使用输入的 conf 配置字典作为参数，渲染模板，得到渲染后的字符串
    fconf_str = template.render(conf if conf else {})

    # 使用 load_yaml_str 函数（这个函数需要你事先定义好），将渲染后的字符串重新加载为字典
    fconf = load_yaml_str(fconf_str)

    # 返回经过 Jinja2 处理之后的配置字典
    return fconf
