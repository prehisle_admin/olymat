import json
from io import BytesIO
from urllib import parse
from pprint import pprint
from urllib.parse import urlparse, parse_qs

import requests

from olymat.utils.control_helper import call_until_success, RetryException


def get_url_param_dict(url):
    """
    获取url上的参数，保存到dict中
    :example
        get_url_param_dict("http://p.com/query?a=1&b=2")["a"] == "1"
    :param url:
    :return:
    """
    p = parse.urlsplit(url)
    params = parse.parse_qsl(p.query, 1)
    params_dict = dict(params)
    return params_dict


def get_post_data_dict(data):
    params = parse.parse_qsl(data, 1)
    params_dict = dict(params)
    return params_dict


def print_post_data(data):
    print("data = {}".format(json.dumps(get_post_data_dict(data), indent=4)))

def print_post_data2(data):
    print("data = {}".format(json.dumps(get_post_data_dict(data), indent=4, ensure_ascii=False)))

def print_multipart_post_data(text):
    for line in text.split("\n"):
        items = line.split()
        print(f'''"{items[0]}": "{items[1] if len(items)>1  else ''}",''')


def print_url_info(url):
    url, query = url.split('?', 1)
    print('url = "{}"'.format(url))
    print("params = {}".format(json.dumps(get_post_data_dict(query), indent=4)))


def print_header_info(header_data):
    lines = header_data.strip().split("\n")
    print("headers = {}".format(json.dumps(dict(map(lambda line: line.split(": "), lines)), indent=4)))

def print_cookie_info(cookie_str):
    cookies = {}
    for item_s in cookie_str.split("; "):
        try:
            pos = item_s.find("=")
            k = item_s[:pos]
            v = item_s[pos+1:]
        except:
            logging.error("", exc_info=True)
            print("[%s]" % item_s)
            assert False, "解析错误"
        cookies[k] = v
    print("cookies = {}".format(json.dumps(cookies, indent=4)))

def print_jsobj_info(jsobj):
    """
authType: "account"
domain: ""

to

jsobj = {
    "authType": "account",
    "domain": ""
}
    """
    lines = jsobj.strip().split("\n")
    r = "jsobj = {}".format(json.dumps(dict(map(lambda line: line.split(": "), lines)), indent=4))
    r = r.replace(r'\"', '')
    print(r)


@call_until_success(retry_exceptions=[requests.exceptions.ConnectionError], try_times=20, sleep_time=5)
def url_get_until_status_code_ok(url, code=200):
    r = requests.get(url, verify=False)
    print(r.text)
    if r.status_code != code:
        raise RetryException()


def print_multipart_req(boundary_str, body):
    import cgi
    pdict = {'boundary': boundary_str}
    d = cgi.parse_multipart(BytesIO(body), pdict)
    d = {k: [None, v[0]] for k, v in d.items()}
    pprint(d)

def test_print_multipart_req():
    param_str = b'''\
------WebKitFormBoundaryZHMk04ZAqjr1cApj
Content-Disposition: form-data; name="data[UserConfig][cert_file]"; filename=""
Content-Type: application/octet-stream

------WebKitFormBoundaryZHMk04ZAqjr1cApj
Content-Disposition: form-data; name="data[_Token][unlocked]"


------WebKitFormBoundaryZHMk04ZAqjr1cApj--
'''
    # Content-Type: application/octet-stream 上传文件需手动加
    print_multipart_req(b"----WebKitFormBoundaryZHMk04ZAqjr1cApj", param_str)


def parse_params_from_url(url):
    parsed_url = urlparse(url)

    # 解析 fragment（URL中的锚点部分），将其转换为字典
    params_dict = parse_qs(parsed_url.fragment)

    # 由于 parse_qs 会将每个值都放在一个列表中，这里将其简化为直接的 key-value 对
    params_dict = {key: value[0] for key, value in params_dict.items()}
    return params_dict
