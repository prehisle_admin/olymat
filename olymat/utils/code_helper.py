import base64
import hashlib
import hmac


def b64encode(data) -> str:
    if isinstance(data, str):
        data = data.encode()
    return base64.b64encode(data).decode()


def b64decode(data) -> bytes:
    if isinstance(data, bytes):
        data = data.decode()
    return base64.b64decode(data)


def sha256_hash(key, message):
    return hmac.new(key.encode('utf-8'), message.encode('utf-8'), hashlib.sha256).hexdigest()


def sm3_hmac(key, data):
    return hmac.new(key.encode('utf-8'), data.encode('utf-8'), digestmod="sm3").hexdigest()
