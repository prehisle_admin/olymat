import os
import queue
import subprocess
import sys
import threading
import time

from olymat.utils.control_helper import RetryException, call_until_success


def winexec(cmd):
    code, output = winexec_ex(cmd)
    return output


def winexec2(cmd):
    code, output = winexec_ex2(cmd)
    return output


def winexec_ex(cmd):
    return subprocess.getstatusoutput(cmd)


def winexec_ex2(cmd):
    r = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='gbk')
    return r.returncode, r.stdout


def is_ping_ok(host):
    code, output = subprocess.getstatusoutput("ping -n 1 -w 1 %s" % host)
    # logging.info("is_ping_ok output[%s]" % output)
    return "TTL=" in output


@call_until_success(try_times=200, sleep_time=2)
def wait_ping_ok(host):
    if not is_ping_ok(host):
        raise RetryException()


@call_until_success(try_times=200, sleep_time=2)
def wait_ping_fail(host):
    if is_ping_ok(host):
        raise RetryException()


class WindowCmdWrap:
    def __init__(self, cmd, shell=True):
        self.pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE,
                                     shell=shell)
        if type(cmd) == str and cmd.startswith("powershell"):
            self.exec("function prompt {'<olym>'}", wait_times=2)
        else:
            # self.exec("chcp 65001 && set prompt=$lolym$g")
            self.exec("set prompt=$lolym$g")

    def exec(self, cmd, wait_str="<olym>", wait_times=1):
        self.pipe.stdin.write((cmd + "\n").encode("gbk"))
        self.pipe.stdin.flush()
        wait_str = wait_str.encode("gbk")
        count = 0
        r = b""
        while True:
            c = self.pipe.stdout.read(1)
            if c:
                r += c
                # print(c.decode("gbk", errors="ignore"), end="", flush=True)
                sys.stdout.buffer.write(c)
                sys.stdout.buffer.flush()
                if r.endswith(wait_str):
                    count += 1
                    if wait_times == count:
                        break
            else:
                break
        return r.decode("gbk", errors="ignore")

    def close(self):
        self.pipe.stdin.close()


class WindowCmdWrap2:
    def __init__(self, cmd, stdin_encoding="utf-8", stdout_encoding="utf-8", powershell=False,
                 stderr=subprocess.STDOUT, shell=True):
        '''
        stderr=sys.stdout.buffer
        '''
        self.stdout_encoding = stdout_encoding
        self.stdin_encoding = stdin_encoding
        self.pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=stderr, stdin=subprocess.PIPE,
                                     bufsize=1,
                                     shell=shell,
                                     encoding=stdin_encoding)

        if powershell:
            self.exec("function prompt {'OLYM>'}", wait_str=">", wait_times=3)
        else:
            self.exec("set prompt=$P$G OLYM$g")

    def __enter__(self):
        print("Entering the with block")
        return self  # 返回一个对象，将会被赋给 as 后面的变量

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print("Exiting the with block")
        if exc_type is not None:
            print(f"An exception of type {exc_type} occurred with message: {exc_value}")
            raise exc_value
        if self.pipe:
            self.kill()
        return True  # 如果返回 True，则会忽略异常，不会抛出异常

    def kill(self):
        # self.exec("\x03")  # 发送Ctrl+C指令
        # os.kill(self.pipe.pid, signal.CTRL_C_EVENT)
        self.pipe.terminate()
        self.pipe.wait()  # 等待子进程结束

    def exec(self, cmd, wait_str="OLYM>", wait_times=1):
        # print(cmd)
        self.pipe.stdin.write(str(cmd) + "\n")
        self.pipe.stdin.flush()
        count = 0
        r = ""
        while True:
            try:
                c = self.pipe.stdout.read(1)
                if c:
                    r += c
                    print(c, end="", flush=True)
                    if r.endswith(wait_str):
                        count += 1
                        if wait_times == count:
                            break
                else:
                    break
            except UnicodeDecodeError:
                pass
        return r


class WindowCmdWrap3:
    def __init__(self, cmd, shell=True):
        '''
        stderr=sys.stdout.buffer
        '''
        self.pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE,
                                     bufsize=1,
                                     shell=shell)

        self.exec("set prompt=$P$G OLYM$g")
        self.exec("chcp 65001")

    def __enter__(self):
        # print("Entering the with block")
        return self  # 返回一个对象，将会被赋给 as 后面的变量

    def __exit__(self, exc_type, exc_value, exc_traceback):
        # print("Exiting the with block")
        if exc_type is not None:
            print(f"An exception of type {exc_type} occurred with message: {exc_value}")
            raise exc_value
        if self.pipe:
            self.kill()
        return True  # 如果返回 True，则会忽略异常，不会抛出异常

    def kill(self):
        # self.exec("\x03")  # 发送Ctrl+C指令
        # os.kill(self.pipe.pid, signal.CTRL_C_EVENT)
        self.pipe.terminate()
        self.pipe.wait()  # 等待子进程结束

    def exec(self, cmd, wait_str=b"OLYM>", encoding="utf-8", wait_times=1):
        if isinstance(wait_str, str):
            wait_str = wait_str.encode(encoding)
        if isinstance(cmd, str):
            cmd = cmd.encode(encoding)
        self.pipe.stdin.write(cmd + "\r\n".encode())
        self.pipe.stdin.flush()
        count = 0
        r = b""
        line = b""
        while True:
            c = self.pipe.stdout.read(1)
            if c:
                r += c
                line += c

                if r.endswith(wait_str):
                    print(line.decode(encoding, errors="ignore"), end="", flush=True)
                    line = b""
                    count += 1
                    if wait_times == count:
                        break
                elif c == b"\n":
                    print(line.decode(encoding, errors="ignore"), end="", flush=True)
                    line = b""
            else:
                break
        return r.decode(encoding, errors="ignore")


class TimeoutException(Exception):
    pass


class WindowsCommandManager:
    def __init__(self, terminal="cmd", shell=True, readsize=1):
        self.readsize = readsize
        self.output_queue = queue.Queue()
        self.error_queue = queue.Queue()
        self.capture_enabled = os.getenv("PYTEST_CAPTURE_TEE_SYS") == "1"

        # 启动终端进程
        self.process = subprocess.Popen(
            [terminal],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=shell,
            bufsize=0,
        )

        # 启动输出线程
        self._start_output_thread(self.process.stdout, self.output_queue)
        self._start_output_thread(self.process.stderr, self.error_queue)

        # 设置UTF-8编码并初始化提示符
        self._initialize_console()

    def _initialize_console(self):
        """初始化控制台环境并获取提示符"""
        # 设置代码页为UTF-8
        self.raw_send("chcp 65001\n")
        # 发送空命令触发新提示符
        self.raw_send("\n")
        # 获取并设置提示符模式
        self.wait_for = self._get_prompt_pattern()
        print(f"\nInitialized console with prompt: {self.wait_for}")

    def _start_output_thread(self, stream, output_queue):
        """启动输出线程"""
        def enqueue_output():
            try:
                while True:
                    chunk = stream.read(self.readsize)
                    if not chunk:
                        break
                    output_queue.put(chunk)
            finally:
                stream.close()

        threading.Thread(target=enqueue_output, daemon=True).start()

    def _get_prompt_pattern(self):
        """获取当前提示符模式"""
        prompt = b""
        while True:
            try:
                chunk = self.output_queue.get(timeout=5)
                prompt += chunk
                if prompt.endswith(b">"):
                    lines = prompt.splitlines()
                    if lines:
                        return lines[-1].strip()
            except queue.Empty:
                raise RuntimeError("Failed to detect command prompt") from None

    def update_readsize(self, readsize):
        self.readsize = readsize

    def raw_send(self, data):
        """发送原始数据到终端"""
        if isinstance(data, str):
            if not data.endswith("\n"):
                data += "\n"
            data = data.encode()
        self.process.stdin.write(data)
        self.process.stdin.flush()

    def raw_except(self, wait_for=None, timeout=None, is_end=True, max_buff=1024*1024):
        """接收输出直到满足条件"""
        output = bytearray()
        wait_for = wait_for or self.wait_for
        wait_for = wait_for if isinstance(wait_for, bytes) else wait_for.encode()
        start_time = time.time()

        while True:
            # 检查超时
            if timeout and time.time() - start_time > timeout:
                raise TimeoutException(f"Timeout after {timeout} seconds")

            # 检查进程状态
            if self.process.poll() is not None:
                self._drain_queue(output)
                break

            # 读取输出
            try:
                chunk = self.output_queue.get(timeout=0.1)
                output += chunk
                self._handle_output(chunk)

                # 检查终止条件
                if (is_end and output.endswith(wait_for)) or (not is_end and wait_for in output):
                    break

                # 处理缓冲区溢出
                if len(output) > max_buff:
                    del output[:-max_buff]

            except queue.Empty:
                continue

        return output.decode("utf-8", errors="ignore")

    def _drain_queue(self, output):
        """清空输出队列"""
        try:
            while True:
                output += self.output_queue.get_nowait()
        except queue.Empty:
            pass

    def _handle_output(self, chunk):
        """处理实时输出"""
        if self.capture_enabled:
            print(chunk.decode("utf-8", errors="ignore"), end="", flush=True)
        else:
            sys.stdout.buffer.write(chunk)
            sys.stdout.flush()

    def run_command(self, command, wait_for=None, timeout=None):
        """执行命令并等待完成"""
        self.raw_send(command)
        return self.raw_except(wait_for, timeout)

    def update_wait_for(self, wait_for):
        self.wait_for = wait_for

    def close(self):
        """关闭终端"""
        try:
            self.process.stdin.write(b"exit\n")
            self.process.stdin.flush()
        except (BrokenPipeError, OSError):
            pass
        finally:
            self.process.terminate()
            self.process.wait()

# 示例用法
if __name__ == "__main__":
    manager = WindowsCommandManager()
    try:
        output = manager.run_command("echo Hello World")
        print("Command output:", output)
    finally:
        manager.close()