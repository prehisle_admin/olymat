import os

from jinja2 import Environment, FileSystemLoader, BaseLoader

from olymat.utils.file_helper import write_file


def gen_data_with_tpl(tpl_file_path, conf):
    env = Environment(loader=FileSystemLoader(os.path.dirname(tpl_file_path)), keep_trailing_newline=True)
    template = env.get_template(os.path.basename(tpl_file_path))
    output = template.render(**conf)
    return output


def gen_data_with_str_tpl(str_tpl, conf):
    template = Environment(loader=BaseLoader).from_string(str_tpl)
    output = template.render(**conf)
    return output


def gen_data_with_str_tpl_keep_tail(str_tpl, conf):
    template = Environment(loader=BaseLoader, keep_trailing_newline=True, newline_sequence="\r\n").from_string(str_tpl)
    output = template.render(**conf)
    return output


def gen_file_by_tpl(tpl_file_path, conf, dst_file):
    data = gen_data_with_tpl(tpl_file_path, conf)
    write_file(dst_file, data.encode("utf-8"))


import shutil


def apply_jinja_to_directory(src_dir, dst_dir, conf):
    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    for root, dirs, files in os.walk(src_dir):
        for file in files:
            src_file_path = os.path.join(root, file)
            relative_path = os.path.relpath(src_file_path, src_dir)
            dst_file_path = os.path.join(dst_dir, relative_path)

            # Ensure the destination directory exists
            os.makedirs(os.path.dirname(dst_file_path), exist_ok=True)

            gen_file_by_tpl(src_file_path, conf, dst_file_path)


