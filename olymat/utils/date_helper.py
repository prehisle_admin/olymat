import time
from datetime import datetime, timedelta


def get_cur_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())


def get_timestamp13():
    return str(int(time.time() * 1000))


def get_timestamp13_int():
    return int(time.time() * 1000)


def get_timestamp10():
    return str(int(time.time()))


def get_timestamp10_int():
    return int(time.time())


def get_now_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())


def get_now_time2():
    return time.strftime("%Y%m%d%H%M%S", time.localtime())


def get_time_delta(days=0, seconds=0, microseconds=0,
                   milliseconds=0, minutes=0, hours=0, weeks=0):
    current_time = datetime.now()
    final_time = current_time + timedelta(days=days, seconds=seconds, microseconds=microseconds,
                                          milliseconds=milliseconds, minutes=minutes, hours=hours, weeks=weeks)
    return final_time.strftime("%Y-%m-%d %H:%M:%S")


def test_time_delta():
    print(f"减去10分钟后的时间：{get_time_delta(minutes=-10)}")
    print(f"6个月后的时间：{get_time_delta(days=180)}")
