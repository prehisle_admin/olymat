import os
import re

from olymat.utils.log_helper import logi


def write_file(file_path, data, mode="wb"):
    with open(file_path, mode) as f:
        return f.write(data)


def append_to_file(file_path, data, mode="a", encoding="utf-8"):
    with open(file_path, mode, encoding=encoding) as f:
        return f.write(data)


def read_file(file_path, mode="rb"):
    with open(file_path, mode) as f:
        return f.read()


def touch(file_path):
    with open(file_path, 'a'):
        os.utime(file_path, None)


def replace_data(file_path, pattern, repl, flags=0):
    logi(f"replace_data {file_path}")
    raw_content = read_file(file_path).decode()
    dst_content = re.sub(pattern, repl, raw_content, flags=flags)
    write_file(file_path, dst_content.encode())


def replace_data_bin(file_path, src_d, dst_d):
    raw_content = read_file(file_path)
    dst_content = raw_content.replace(src_d, dst_d)
    write_file(file_path, dst_content)
