import subprocess
from datetime import datetime

from olymat.utils.log_helper import loge


def get_git_info(short_hash=True):
    git_info = {}
    try:
        # 获取提交哈希
        hash_command = ['git', 'rev-parse']
        if short_hash:
            hash_command.append('--short')
        hash_command.append('HEAD')
        git_info['hash'] = subprocess.check_output(hash_command).strip().decode('utf-8')

        # 获取提交时间（指定格式，包含秒）
        git_commit_time_str = subprocess.check_output(
            ['git', 'log', '-1', '--date=format:%Y-%m-%d %H:%M:%S', '--format=%ci']).strip().decode('utf-8')

        # 解析时间字符串，去掉秒和时区
        git_info['commit_time'] = datetime.strptime(git_commit_time_str, "%Y-%m-%d %H:%M:%S %z").strftime(
            "%Y-%m-%d %H:%M")

        # 获取当前分支名称
        git_info['branch'] = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode(
            'utf-8')

        # 获取当前分支下的提交总次数
        git_info['commit_count'] = subprocess.check_output(['git', 'rev-list', '--count', 'HEAD']).strip().decode(
            'utf-8')

        # 获取最近一次提交的 message
        git_info['commit_message'] = subprocess.check_output(['git', 'log', '-1', '--pretty=%B']).strip().decode(
            'utf-8')

        return git_info
    except subprocess.CalledProcessError as e:
        loge(e)
        return None


def test_get():
    git_info = get_git_info()

    if git_info:
        print("Short hash:", git_info.get('hash'))
        print("Commit time:", git_info.get('commit_time'))
        print("Branch:", git_info.get('branch'))
        print("Commit count:", git_info.get('commit_count'))
        print("Commit message:", git_info.get('commit_message'))
