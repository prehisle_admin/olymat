import concurrent.futures
import logging
import threading
import time


def olymat_threads_run(thread_funcs_dict, max_workers=16):
    r = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        future_to_index = {executor.submit(func, *args): k for k, (func, args) in thread_funcs_dict.items()}
        for future in concurrent.futures.as_completed(future_to_index):
            name = future_to_index[future]
            return_value = future.result()
            r[name] = return_value
    return r


def thread_sleep_set(sleep_sec, event):
    time.sleep(sleep_sec)
    logging.info("thread_sleep_set trigger")
    event.set()


class OlymatThreadRunCTX:
    def __init__(self):
        self.stop_event = threading.Event()

    def is_running(self):
        return not self.stop_event.is_set()

    def stop(self):
        self.stop_event.set()


class OlymatThreadRun:
    def __init__(self, max_workers):
        self.ctx = OlymatThreadRunCTX()
        self.tasks = {}
        self.max_workers = max_workers

    def add_task(self, name, task_func, *args):
        self.tasks[name] = (task_func, (self.ctx, *args))

    def run(self, stop_timeout=10):
        if stop_timeout:
            threading.Thread(target=thread_sleep_set, args=(stop_timeout, self.ctx.stop_event)).start()
        return olymat_threads_run(self.tasks, self.max_workers)
