import os
import pkgutil
from importlib.machinery import SourceFileLoader


def get_user_plugins_dir():
    return os.path.join(os.getcwd(), "plugins")


def get_inner_plugins_dir():
    return os.path.join(os.path.dirname(__file__), "..", "plugins")


def get_plugins_by_path(plugins_path, plugin_type):
    discovered_plugins = {
        name: SourceFileLoader(name + "_" + plugin_type, os.path.join(plugins_path, "{}.py".format(name))).load_module()
        for finder, name, ispkg in pkgutil.iter_modules([plugins_path])
    }
    return discovered_plugins


def get_plugins_by_name(plugin_name):
    inner_plugins = get_plugins_by_path(os.path.join(get_inner_plugins_dir(), plugin_name), plugin_name)
    user_plugins = get_plugins_by_path(os.path.join(get_user_plugins_dir(), plugin_name), plugin_name)
    plugins = inner_plugins
    plugins.update(user_plugins)
    return plugins


def get_all_plugins():
    plugins_dict = {
        "in_end": get_plugins_by_name("in_end"),
        "out_end": get_plugins_by_name("out_end"),
        "testid_gen": get_plugins_by_name("testid_gen"),
    }
    return plugins_dict
