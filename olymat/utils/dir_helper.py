import glob

import os
import shutil
import time


def make_rel_path(rel_path, *args):
    return os.path.join(os.path.dirname(rel_path), *args)


def make_work_path(rel_file_path, *args):
    return os.path.join(os.path.dirname(rel_file_path), *args)


def reset_dir(dir_path):
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)
    os.makedirs(dir_path, exist_ok=True)


def get_dir_file_list(bin_path):
    r = os.listdir(bin_path)
    return list(map(lambda x: os.path.join(bin_path, x), r))


def has_extension(path):
    _, extension = os.path.splitext(path)
    return bool(extension)


def make_path_fix(*args, auto_create=True):
    final_path = os.path.abspath(os.path.join(*args))
    if auto_create:
        if has_extension(final_path):
            os.makedirs(os.path.dirname(final_path), exist_ok=True)
        else:
            os.makedirs(final_path, exist_ok=True)

    return final_path


def find_files(directory, pattern='*.img.gz'):
    # 使用 glob 模块查找指定目录下的所有以 .img.gz 结尾的文件
    pattern = os.path.join(directory, pattern)
    imggz_files = glob.glob(pattern)

    return imggz_files

def delete_old_files(dir_path, days):
    """
    删除指定目录下所有 n 天前创建的文件

    Args:
        dir_path: 指定目录的路径
        days: 要删除的文件的创建时间距离当前时间的间隔天数

    Returns:
        无

    """

    os.makedirs(dir_path, exist_ok=True)

    # 获取指定目录下的所有文件和文件夹列表
    file_list = os.listdir(dir_path)

    # 遍历文件和文件夹列表
    for file_name in file_list:
        # 获取文件或文件夹的绝对路径
        file_path = os.path.join(dir_path, file_name)

        # 如果是文件，且创建时间距离当前时间超过指定的天数，则删除
        if os.path.isfile(file_path):
            last_modified_time = os.path.getmtime(file_path)
            if time.time() - last_modified_time > days * 24 * 3600:
                os.remove(file_path)

        # 如果是文件夹，则递归调用本函数
        elif os.path.isdir(file_path):
            delete_old_files(file_path, days)