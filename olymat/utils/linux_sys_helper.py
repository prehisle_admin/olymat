import logging
import re
from collections import OrderedDict

import paramiko

from olymat.utils.control_helper import call_until_success, RetryException
from olymat.utils.date_helper import get_cur_time


def get_proc_fd_cmd(proc_id):
    return 'ls /proc/%s/fdinfo -l | wc -l' % proc_id


def get_proc_ids_cmd(proc_name):
    return 'ps -ef | grep --colour=never "%s" | grep -v "grep" | awk -F " " \'{print $2}\'' % proc_name


def get_proc_mem_cmd(proc_id):
    return "cat /proc/%s/status | grep --colour=never VmRSS" % proc_id


def get_proc_ids(ssh, proc_name):
    output = ssh.exec_sudo(get_proc_ids_cmd(proc_name))
    return list(map(int, output.split()))


def get_first_proc_id(ssh, proc_name):
    return get_proc_ids(ssh, proc_name)[0]


@call_until_success(try_times=5)
def get_proc_fd(ssh, proc_id):
    cmd = get_proc_fd_cmd(proc_id)
    output = ssh.exec_sudo(cmd)
    logging.info("cmd [%s]\noutput [%s]" % (cmd, output))
    if "No such file or directory" in output:
        raise RetryException()
    return int(output)


def get_proc_mem(ssh, proc_id):
    cmd = get_proc_mem_cmd(proc_id)
    output = ssh.exec_sudo(cmd)
    return int(output.split()[1])


def get_proc_VmRSS(ssh, proc_id):
    cmd = "cat /proc/%s/status | grep --colour=never VmRSS" % proc_id
    output = ssh.exec_sudo(cmd)
    return int(output.split()[1])


def get_proc_VmSize(ssh, proc_id):
    cmd = "cat /proc/%s/status | grep --colour=never VmSize" % proc_id
    output = ssh.exec_sudo(cmd)
    return int(output.split()[1])


def get_proc_RssAnon(ssh, proc_id):
    cmd = "cat /proc/%s/status | grep --colour=never RssAnon" % proc_id
    output = ssh.exec_sudo(cmd)
    return int(output.split()[1])


def get_proc_status(ssh, proc_id):
    cmd = "cat /proc/%s/status" % proc_id
    output = ssh.exec_sudo(cmd)
    return output


def get_proc_info(ssh, proc_name):
    pid = get_first_proc_id(ssh, proc_name)
    return OrderedDict({
        "time": get_cur_time(),
        "proc_name": proc_name,
        "pid": pid,
        "proc_mem": get_proc_mem(ssh, pid),
        "proc_fd": get_proc_fd(ssh, pid),
    })


def get_proc_info_ex(ssh, proc_name):
    pid = get_first_proc_id(ssh, proc_name)
    return OrderedDict({
        "time": get_cur_time(),
        "proc_name": proc_name,
        "pid": pid,
        "proc_VmRSS": get_proc_VmRSS(ssh, pid),
        "proc_VmSize": get_proc_VmSize(ssh, pid),
        "proc_RssAnon": get_proc_RssAnon(ssh, pid),
        "proc_fd": get_proc_fd(ssh, pid),
    })


def get_proc_status_by_key(key, status_str):
    return re.findall(f"^{key}:(.*?)$", status_str, re.MULTILINE)[0].strip().split()[0]


def get_proc_info_ex2(ssh, proc_name, status_keys=["VmHWM", "VmRSS", "VmPeak", "VmSize", "RssAnon",
                                                   "VmData", "VmSwap", "Threads"]):
    pid = get_first_proc_id(ssh, proc_name)
    r = get_proc_status(ssh, pid)
    proc_infos = {key: get_proc_status_by_key(key, r) for key in status_keys}
    rd = OrderedDict({
        "time": get_cur_time(),
        "proc_name": proc_name,
        "pid": pid,
        "proc_fd": get_proc_fd(ssh, pid),
    })
    rd.update(proc_infos)
    return rd


def get_disk_use(ssh):
    output = ssh.exec_sudo("df -k")
    return int(output.splitlines()[3].split()[3])


def get_disk_use_ex(ssh, dev_name):
    output = ssh.exec_sudo(f"df -k {dev_name}")
    return int(output.splitlines()[1].split()[3])


def get_cpu_utilization(ssh):
    cmd = "top -b -n1"
    output = ssh.exec_sudo(cmd)
    # logging.info("get_cpu_utilization output [%s]" % output.splitlines()[2])
    return 100 - float(output.splitlines()[2].split(",")[3].split()[0])


def get_cpu_utilization2(ssh):
    cmd = "nice -n -20 top -b -d 1 -n1"
    try:
        output = ssh.exec(cmd)
        return 100 - float(output.splitlines()[2].split(",")[3].split()[0])
    except IndexError:
        return 0
    except paramiko.ssh_exception.ChannelException:
        return 100001


def get_mem_utilization(ssh):
    cmd = "free -m"
    output = ssh.exec_sudo(cmd)
    values = output.splitlines()[1].split()
    total = int(values[1])
    available = int(values[6])
    return (total - available) / total * 100


def get_sys_info(ssh):
    return OrderedDict({
        "time": get_cur_time(),
        "cpu_utilization": "%.02f" % get_cpu_utilization(ssh),
        "mem_utilization": "%.02f" % get_mem_utilization(ssh),
        "available_disk": get_disk_use(ssh),
    })


def get_sys_info_ex(ssh, dev_name):
    return OrderedDict({
        "time": get_cur_time(),
        "cpu_utilization": "%.02f" % get_cpu_utilization(ssh),
        "mem_utilization": "%.02f" % get_mem_utilization(ssh),
        "available_disk": get_disk_use_ex(ssh, dev_name),
    })
