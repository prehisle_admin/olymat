from olymat.bases.mgr_base import MgrBase


class Base:
    ALLOW_EMPTY_CONF = False

    def __init__(self, conf, mgr: MgrBase):
        self.mgr = mgr
        self.conf = conf

    def init(self):
        pass
