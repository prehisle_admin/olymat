class MgrBase:
    def get_testid(self):
        return "MgrBase get_testid"

    def get_http_proxy(self):
        return None

    def get_user_agent(self):
        return None

    def get_work_data_dir(self):
        return './work_data'

    def get_case_extra(self):
        return "MgrBase get_case_extra"

    def get_in_end_infos(self):
        pass
