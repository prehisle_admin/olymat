from .base import Base
from .mgr_base import MgrBase


class InEndBase(Base):
    def __init__(self, conf, mgr: MgrBase):
        super().__init__(conf, mgr)
        self.data = {}

    def update_inend_data(self):
        pass

    def get_inend_data(self):
        return {}
