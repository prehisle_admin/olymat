from .base import Base


class OutEndBase(Base):
    def report_test_result(self, data):
        pass

    def report_other_info(self, data):
        pass

    def sessionstart(self):
        pass

    def sessionfinish(self, exitstatus):
        pass
