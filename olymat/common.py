from olymat.mgr import mgr
from olymat.utils.olym_requests import new_session, new_session_v2


def get_olym_s(**kwargs):
    conf = mgr.get_main_conf()
    return new_session(conf['http_proxy'], conf['user_agent'], **kwargs)


def get_olym_s_v2(**kwargs):
    conf = mgr.get_main_conf()
    return new_session_v2(conf['http_proxy'], conf['user_agent'], **kwargs)
