import logging
import os
import sys
from collections import defaultdict

import yaml

from olymat.bases.in_end import InEndBase
from olymat.bases.mgr_base import MgrBase
from olymat.bases.out_end import OutEndBase
from olymat.utils.plugin_mgr import get_all_plugins
from olymat.utils.utils import update

def clean_sys_python311_path():
    # 当系统中安装了 python3.11时，可能会和olymat_cli中的python冲突，python会自动读取注册表中的路径加到sys.path中
    import sys
    if sys.platform == 'win32':
        import winreg


        def get_python_install_path():
            key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r"SOFTWARE\Python\PythonCore\3.11\InstallPath")
            return winreg.QueryValueEx(key, "")[0]


        def clear_sys_path():
            install_path = get_python_install_path()
            sys.path = [p for p in sys.path if not p.startswith(install_path[:-1])]


        try:
            clear_sys_path()
        except Exception as e:
            pass


# clean_sys_python311_path()

class MGR(MgrBase):
    def __init__(self):
        self.conf = {}
        if os.path.isfile(r".\conf.yaml"):
            with open(r".\conf.yaml", encoding="utf-8") as f:
                self.conf = yaml.load(f, Loader=yaml.SafeLoader)
        with open(os.path.join(os.path.dirname(__file__), "default_conf.yaml"), encoding="utf-8") as f:
            self.default_conf = yaml.load(f, Loader=yaml.SafeLoader)
        update(self.default_conf, self.conf)  # 处理缺失配置
        self.conf = self.default_conf

        if "enable_gevent_patch_all" in self.conf and self.conf["enable_gevent_patch_all"]:
            import gevent.monkey
            gevent.monkey.patch_all()

        self.conf["main"] = defaultdict(lambda: None, self.conf["main"])
        # 默认不建立工作目录
        if self.conf['main']['work_data_dir'] and not os.path.exists(self.conf['main']['work_data_dir']):
            os.makedirs(os.path.join(self.conf['main']['work_data_dir'], "logs"))
        self.plugins = get_all_plugins()
        self.plugins_init()
        self.testid_gen = list(self.plugin_objs_dict['testid_gen'].values())[0]
        if "env_name" in self.conf['main']:
            self.testid_gen.prefix = self.conf['main']['env_name']
        self.update_in_end_infos()

    def plugins_init(self):
        self.plugin_objs_dict = defaultdict(dict)
        for plugin_type in self.conf['plugins']:
            self.plugin_objs_dict[plugin_type] = {}
            for name in self.conf['plugins'][plugin_type]:
                conf = self.conf[plugin_type][name] if name in self.conf[plugin_type] else None
                self.plugin_objs_dict[plugin_type][name] = self.plugins[plugin_type][name].Plugin(conf,
                                                                                                  self)
        for plugin_type in self.conf['plugins']:
            for name in self.conf['plugins'][plugin_type]:
                self.plugin_objs_dict[plugin_type][name].init()

    def report_test_result(self, data):
        for out_end in self.plugin_objs_dict['out_end'].values():
            out_end.report_test_result(data)

    def get_testid(self):
        return self.testid_gen.get_testid()

    def inc_testid(self):
        return self.testid_gen.inc_testid()

    def update_testid(self, date, count):
        self.testid_gen.update_testid(date, count)

    def get_work_data_dir(self):
        return self.conf['main']['work_data_dir'] if self.conf['main']['work_data_dir'] else 'work_data'

    def get_log_file_path(self):
        return os.path.join(self.get_work_data_dir(), "logs",
                            self.get_testid() + ".log")

    def get_in_ends(self):
        return self.plugin_objs_dict['in_end']

    def get_out_ends(self):
        return self.plugin_objs_dict['out_end']

    def get_main_conf(self):
        return self.conf["main"]

    def get_in_end_by_name(self, name) -> InEndBase:
        return self.plugin_objs_dict['in_end'][name]

    def get_out_end_by_name(self, name) -> OutEndBase:
        return self.plugin_objs_dict['out_end'][name]

    def get_in_end_data(self, name):
        return self.get_in_end_by_name(name).get_inend_data()

    def get_case_extra(self):
        return self.conf["main"]['case_extra']

    def get_http_proxy(self):
        return self.conf["main"].get("http_proxy", None)

    def get_user_agent(self):
        return self.conf["main"]['user_agent']

    def update_in_end_infos(self):
        r = {}
        for in_end in self.get_in_ends():
            data = self.get_in_end_data(in_end)
            r.update(data)
        self.in_end_infos = r

    def get_in_end_infos(self):
        return self.in_end_infos

    def sessionstart(self):
        for k, v in self.get_out_ends().items():
            v.sessionstart()

    def sessionfinish(self, exitstatus):
        for k, v in self.get_out_ends().items():
            v.sessionfinish(exitstatus)

    def is_disable_olymat(self):
        return self.conf["main"].get("disable_olymat", False)

    def get_default_conf_name(self):
        if "default_conf_name" in self.conf["main"]:
            return self.conf["main"]["default_conf_name"]
        else:
            return "default"

    def get_conf(self):
        return self.conf

    def set_conf(self, conf):
        self.conf = conf


mgr = MGR()
olymat_mgr = mgr


def inc_testid():
    mgr.inc_testid()
    new_testid = mgr.get_testid()
    logging.info("new testid is %s" % new_testid)
    return new_testid
