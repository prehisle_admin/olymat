# import sys
# import io

# sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8', write_through=True)
# sys.stderr = io.TextIOWrapper(sys.stderr.buffer, encoding='utf-8', write_through=True)
#
#
# class Unbuffered(object):
#     def __init__(self, stream):
#         self.stream = stream
#
#     def write(self, data):
#         self.stream.write(data)
#         self.stream.flush()
#
#     def writelines(self, datas):
#         self.stream.writelines(datas)
#         self.stream.flush()
#
#     def __getattr__(self, attr):
#         return getattr(self.stream, attr)
#
#
# sys.stdout = Unbuffered(sys.stdout)
# sys.stderr = Unbuffered(sys.stderr)

import inspect
import os
import re
import time
from collections import OrderedDict
from pathlib import Path

import pytest
from _pytest.config import hookimpl

from olymat.common import get_olym_s
from olymat.mgr import mgr
from olymat.utils.utils import func_run_once
from olymat.utils.yaml_helper import load_yaml_str, import_parse_yaml, render_yaml

case_info_key_default = {
    'start_time': 'not set',
    'caseId': 'not set',
    'name': 'not set',
    'step': 'not set',
    'test_result': 'not set',
    'severity': 'normal',
    'duration': 'not set',
    'func_ids': '',
    'interface_ids': '',
    'tags': '',
    'issue': '',
    'desc': '',
}

option = None


def pytest_sessionstart(session):
    '''Called after the Session object has been created and before performing collection and entering the run test loop.'''
    import faulthandler
    faulthandler.disable()  # 禁止报错Windows fatal exception: access violation
    session.failednames = set()
    if not option.disable_olymat and not mgr.is_disable_olymat():
        mgr.sessionstart()


def pytest_sessionfinish(session, exitstatus):
    """Called after whole test run finished, right before returning the exit status to the system.

    :param pytest.Session session: The pytest session object.
    :param int exitstatus: The status which pytest will return to the system.
    """
    if not option.disable_olymat and not mgr.is_disable_olymat():
        mgr.sessionfinish(exitstatus)


def report_test_result(item, report, test_result):
    item.olymai_info.update({
        "duration": "%.02f" % report.duration,
        "test_result": test_result,
        "end_time": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),

    })
    data = case_info_key_default.copy()
    data.update(item.olymai_info)

    mgr.report_test_result(data)


def get_case_infos(item):
    d = OrderedDict({})
    markers = list(item.iter_markers(name="case_infos"))
    if not len(markers) > 0:
        return d
    marker = markers[0]
    d.update(marker.kwargs)
    if len(marker.args) > 0:
        d.update(load_yaml_str(marker.args[0]))
    return d


def pytest_collection_modifyitems(items):
    for item in items:
        case_infos = get_case_infos(item)
        if len(case_infos):
            item._nodeid = item.nodeid
            append_str = "  [" + "－".join(case_infos[key] if key in case_infos else "" for  key in ["caseId", "name", "step"]) + "]"
            item._nodeid += append_str
        else:
            item._nodeid = item.nodeid
        item.olymai_info = case_infos


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()

    if option.disable_olymat or mgr.is_disable_olymat():
        return


    if report.when == "setup":
        item.olymai_info.update({
            "testid": mgr.get_testid(),
            "nodeid": item._nodeid,
            "start_time": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),
        })

        if report.skipped:
            report_test_result(item, report, "skipped")
    elif report.when == "call":
        if hasattr(report, "wasxfail"):
            test_result = "xpassed" if report.passed else "xfailed"
        else:
            test_result = report.outcome
        report_test_result(item, report, test_result)



def pytest_addoption(parser):
    parser.addoption("--disable-olymat", action="store_true", default=False,
                     help="")
    parser.addoption("--config", action="store", default=None)


CONF_NAME_INNER = "default"
G_OLYMAT_CONF = {}


def get_conf_name():
    return CONF_NAME_INNER


@pytest.fixture(scope="session")
def CONF_NAME():
    return get_conf_name()


def merge_dicts(dict1, dict2):
    result = dict1.copy()
    for key, value in dict2.items():
        if isinstance(value, dict) and key in result and isinstance(result[key], dict):
            result[key] = merge_dicts(result[key], value)
        else:
            result[key] = value
    return result


@func_run_once
def get_conf(config_name=None):
    caller_file_path = inspect.stack()[2][1]  # 取config.py的全路径
    print(f"get_conf caller_file_path {caller_file_path}")
    cwd = os.getcwd()
    if caller_file_path.endswith("conftest.py"):
        return None  # 测试集中未加载配置, 直接在根conftest.py加载的配置，这里要忽略
    else:
        relpath = os.path.relpath(caller_file_path, start=cwd)
        tests_dir_name = Path(relpath).parts[0]
        G_OLYMAT_CONF["tests_dir_name"] = tests_dir_name
        if not config_name:
            config_name = CONF_NAME_INNER
        conf_file_path = os.path.join(cwd, tests_dir_name, "confs",
                                      config_name if config_name.endswith(
                                          ".yaml") else config_name + ".yaml")
        if not os.path.exists(conf_file_path):
            print(f"\033[91mThe file does not exist, please check. {conf_file_path}\033[0m")
            exit(1)

        conf_step_1 = import_parse_yaml(conf_file_path)
        conf_step_1 = conf_step_1 if conf_step_1 else {}
        if conf_step_1.get("disable_render_yaml", False): # 停用yaml jinja2渲染
            conf = conf_step_1
        else:
            conf = render_yaml(conf_step_1)

    conf = conf if conf else {}

    main_conf = mgr.get_conf()
    if main_conf.get("user_config_priority", True):
        result_conf = merge_dicts(main_conf, conf)
    else:
        result_conf = merge_dicts(conf, main_conf)
    result_conf["_ctx"] = {}
    result_conf["_ctx"]["tests_dir_name"] = tests_dir_name
    result_conf["_ctx"]["config_name"] = config_name
    mgr.set_conf(result_conf)
    return result_conf


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    config.addinivalue_line("markers", "case_infos(**kwargs): this marker takes arguments.")
    global option
    option = config.option


@hookimpl(tryfirst=True)
def pytest_load_initial_conftests(early_config, parser, args):
    global CONF_NAME_INNER
    print(f"pytest_load_initial_conftests {args}")
    CONF_NAME_INNER = vars(parser.parse(args)).get("config")
    if not CONF_NAME_INNER:
        CONF_NAME_INNER = mgr.get_default_conf_name()
        print(f"Configuration file not specified; defaulting to the file '{CONF_NAME_INNER}'.")


def pytest_runtest_setup(item):
    if item.nodeid[:item.nodeid.find("[")] in item.session.failednames \
            and os.path.basename(item.nodeid).startswith("test_xfail_skip"):  # 用于性能压测, 未达到则跳过余下的用例
        pytest.skip("previous test failed (%s)" % item.nodeid)  # or use pytest.xfail like in the other answer


@pytest.fixture(scope="session")
def olymat_mgr():
    return mgr


@pytest.fixture(scope="session")
def olymat_conf():
    return mgr.get_main_conf()


@pytest.fixture(scope="session")
def olymat_s():
    return get_olym_s()


@pytest.fixture(scope="module")
def s_module():
    return get_olym_s()


@pytest.fixture(scope="session")
def s():
    return get_olym_s()


@pytest.fixture(scope="session")
def olymat_in_end_infos():
    return mgr.get_in_end_infos()
