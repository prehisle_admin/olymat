@echo off
chcp 65001
set PYTHONIOENCODING=utf-8
set PYTHONUNBUFFERED=1

:: 删除 Python 3.11 注册表项（需要管理员权限）
reg delete "HKEY_CURRENT_USER\SOFTWARE\Python\PythonCore\3.11" /f >nul 2>&1
if %ERRORLEVEL% EQU 0 (
    echo Python 3.11 registry entries removed successfully.
) else (
    echo No Python 3.11 registry entries found or unable to remove.
)

:: 设置 olymat 路径
set olymat_path=%~dp0python;%~dp0python\Scripts;%~dp0min-git\libexec\git-core
set path=%olymat_path%;%path%

:: 执行程序
olymat_cli.exe %*
