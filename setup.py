from setuptools import setup, find_packages

setup(
    name="olymat",
    version=open("version").readline().strip(),
    description="olym autotest framework",
    long_description=open("README.md", encoding="utf-8").read(),
    author="dayun",
    author_email="prehisle@gmail.com",
    url="",
    packages=find_packages(),
    entry_points={
        "pytest11": [
            "olymat = olymat.plugin"
        ],
        'console_scripts': [
            'olymat = olymat.main:main',
            'olymat_cli = olymat.olymat_cli:main',
        ]
    },
    install_requires=[
        "pytest>=6.2",
        "flask>=2.0.0",
        "requests>=2.2.0",
        "paramiko>=2.7.0",
        "scp==0.14.0",
        "pytest-repeat",
        "PyYAML==6.0.1"
    ],
    license="MIT",
    keywords="",
    python_requires=">=3.6",
    classifiers=[
        "Framework :: Pytest"
    ],
    include_package_data=True,
    package_data={

    }
)

